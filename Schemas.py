from schema import Schema, And, Use, Or, Optional, Regex
from datetime import datetime
import Constants as cs

import re


class APISchema:
    get_journal_record_schema = [
        And(int, Use(lambda e: {'id_journal_record': e})),
        And(int, Use(lambda e: {'id_vdi_user': e})),
        And(Or(int, None), Use(lambda e: {'id_object': e})),
        And(Or(int, None), Use(lambda e: {'id_action': e})),
        And(Or(str, None), Use(lambda e: {'current_object': e})),
        And(Or(datetime, None), Use(lambda e: {'created_at': e})),
        And(bool, Use(lambda e: {'success': e})),
        And(str, Use(lambda e: {'description': e})),
    ]

    get_journal_record_data_schema = [
        And(int, Use(lambda e: {'id_journal_record': e})),
        And(Or(int, None), Use(lambda e: {'id_vdi_user': e})),
        And(Or(str, None), Use(lambda e: {'user_name': e})),
        And(int, Use(lambda e: {'id_client': e})),
        And(str, Use(lambda e: {'client_ip': e})),
        And(Or(str, None), Use(lambda e: {'client_name': e})),
        And(Or(int, None), Use(lambda e: {'id_object': e})),
        And(Or(int, None), Use(lambda e: {'id_action': e})),
        And(Or(str, None), Use(lambda e: {'current_object': e})),
        And(Or(datetime, None), Use(lambda e: {'created_at': e})),
        And(bool, Use(lambda e: {'success': e})),
        And(str, Use(lambda e: {'description': e})),
    ]

    get_client_schema = [
        And(int, Use(lambda e: {'id_client': e})),
        And(str, Use(lambda e: {'client_ip': e})),
        And(Or(str, None), Use(lambda e: {'client_mac': e})),
        And(Or(str, None), Use(lambda e: {'client_name': e})),
        And(Or(str, None), Use(lambda e: {'client_type': e})),
    ]

    get_user_schema = [
        And(int, Use(lambda e: {'id_vdi_user': e})),
        And(Or(Regex(r'\w+'), None), Use(lambda e: {'name': e})),
        And(Or(Regex(r'\w+'), None), Use(lambda e: {'last_name': e})),
        And(Or(Regex(r'\w+'), None), Use(lambda e: {'email': e})),
        And(Regex(r'\w+'), Use(lambda e: {'user_name': e})),
        And(Regex(r'\w+'), Use(lambda e: {'password': e})),
        And(Or(Regex(r'\w+'), None), Use(lambda e: {'state': e})),
        And(datetime, Use(lambda e: {'created_at': e})),
    ]

    search_token_schema = [
        And(Regex(r'\w+'), Use(lambda e: {'id_token': e})),
        And(int, Use(lambda e: {'id_vdi_user': e})),
        And(datetime, Use(lambda e: {'created_at': e})),
        And(Or(datetime, None), Use(lambda e: {'expired_at': e})),
    ]

    get_role_template_schema = [
        And(int, Use(lambda e: {'id_role_template': e})),
        And(int, Use(lambda e: {'id_role_type': e})),
    ]

    get_role_permission_schema = [
        And(int, Use(lambda e: {'id_role_permission': e})),
        And(int, Use(lambda e: {'id_permission': e})),
        And(int, Use(lambda e: {'id_role_template': e})),
    ]

    get_user_role_list_schema = [
        And(int, Use(lambda e: {'id_role': e})),
        And(str, Use(lambda e: {'role_name': e})),
        And(Or(int, None), Use(lambda e: {'id_pool': e})),
        And(Or(str, None), Use(lambda e: {'pool_name': e})),
    ]

    get_user_list_schema = [
        And(int, Use(lambda e: {'id_vdi_user': e})),
        And(Or(Regex(r'\w+'), None), Use(lambda e: {'name': e})),
        And(Or(Regex(r'\w+'), None), Use(lambda e: {'last_name': e})),
        And(Or(Regex(r'\w+'), None), Use(lambda e: {'email': e})),
        And(Regex(r'\w+'), Use(lambda e: {'user_name': e})),
        And(datetime, Use(lambda e: {'created_at': e})),
    ]

    get_permission_schema = [
        And(int, Use(lambda e: {'id_permission': e})),
        And(int, Use(lambda e: {'id_object': e})),
        And(int, Use(lambda e: {'id_action': e})),
        And(Or(str, None), Use(lambda e: {'permission_name': e})),
    ]

    get_user_permission_list_schema = [
        And(Or(int, None), Use(lambda e: {'id_pool': e})),
        And(int, Use(lambda e: {'id_permission': e})),
        And(int, Use(lambda e: {'id_object': e})),
        And(int, Use(lambda e: {'id_action': e})),
    ]

    get_role_schema = [
        And(int, Use(lambda e: {'id_role': e})),
        And(Or(int, None), Use(lambda e: {'id_pool': e})),
        And(int, Use(lambda e: {'id_role_template': e})),
        And(Or(str, None), Use(lambda e: {'role_name': e})),
    ]

    get_role_data_schema = [
        And(int, Use(lambda e: {'id_role': e})),
        And(Or(str, None), Use(lambda e: {'role_name': e})),
        And(Or(int, None), Use(lambda e: {'id_pool': e})),
        And(Or(str, None), Use(lambda e: {'pool_name': e})),
        And(int, Use(lambda e: {'id_role_type': e})),
        And(int, Use(lambda e: {'id_permission': e})),
        And(Or(str, None), Use(lambda e: {'permission_name': e})),
        And(int, Use(lambda e: {'id_object': e})),
        And(int, Use(lambda e: {'id_action': e})),
    ]

    get_role_instance_schema = [
        And(int, Use(lambda e: {'id_role': e})),
        And(Or(int, None), Use(lambda e: {'id_pool': e})),
        And(int, Use(lambda e: {'id_role': e})),
    ]

    get_server_schema = [
        And(int, Use(lambda e: {'id_server': e})),
        And(str, Use(lambda e: {'server_name': e})),
        And(str, Use(lambda e: {'ip': e})),
        And(Regex(r'\w+'), Use(lambda e: {'login': e})),
        And(Regex(r'\w+'), Use(lambda e: {'password': e})),
        And(Or(Regex(r'\w+'), None), Use(lambda e: {'ad_login': e})),
        And(Or(Regex(r'\w+'), None), Use(lambda e: {'ad_password': e})),
        And(Or(int, None), Use(lambda e: {'id_pool': e})),
    ]

    get_server_data_schema = [
        And(int, Use(lambda e: {'id_server': e})),
        And(str, Use(lambda e: {'server_name': e})),
        And(str, Use(lambda e: {'ip': e})),
        And(int, Use(lambda e: {'active': e})),
        And(int, Use(lambda e: {'suspend': e})),
        And(int, Use(lambda e: {'blocked': e})),
        And(Or(int, None), Use(lambda e: {'id_pool': e})),
        And(bool, Use(lambda e: {'connected_2_broker': e})),
        And(Or(str, None), Use(lambda e: {'pool_name': e})),
    ]

    get_server_sessions_schema = [
        And(str, Use(lambda e: {'id_session': e})),
    ]

    get_session_schema = [
        And(str, Use(lambda e: {'id_session': e})),
        And(int, Use(lambda e: {'id_vdi_user': e})),
        And(int, Use(lambda e: {'id_client': e})),
        And(int, Use(lambda e: {'id_server': e})),
        And(Or(datetime, None), Use(lambda e: {'created_at': e})),
        And(Or(datetime, None), Use(lambda e: {'updated_at': e})),
        And(int, Use(lambda e: {'state': e})),
    ]

    get_session_data_schema = [
        And(str, Use(lambda e: {'id_session': e})),
        And(int, Use(lambda e: {'id_vdi_user': e})),
        And(int, Use(lambda e: {'id_client': e})),
        And(int, Use(lambda e: {'id_server': e})),
        And(Or(datetime, None), Use(lambda e: {'created_at': e})),
        And(Or(datetime, None), Use(lambda e: {'updated_at': e})),
        And(int, Use(lambda e: {'state': e})),
        And(str, Use(lambda e: {'server_name': e})),
        And(str, Use(lambda e: {'user_name': e})),
        And(str, Use(lambda e: {'client_ip': e})),
        And(str, Use(lambda e: {'client_name': e})),
    ]

    get_pool_schema = [
        And(int, Use(lambda e: {'id_pool': e})),
        And(str, Use(lambda e: {'pool_name': e})),
        And(bool, Use(lambda e: {'connected_2_broker': e})),
        And(str, Use(lambda e: {'description': e})),
        And(str, Use(lambda e: {'domain': e})),
    ]

    get_pool_data_schema = [
        And(int, Use(lambda e: {'id_pool': e})),
        And(str, Use(lambda e: {'pool_name': e})),
        And(bool, Use(lambda e: {'connected_2_broker': e})),
        And(str, Use(lambda e: {'description': e})),
        And(str, Use(lambda e: {'domain': e})),
        And(int, Use(lambda e: {'servers': e})),
    ]

    sign_in_schema = Schema({'Authorization': And(Regex(r'\w*:\w*'), Use(
        lambda e: {'user_name': re.compile(r'(\w*):(\w*)').findall(e)[0][0],
                   'hashed_pass': re.compile(r'(\w*):(\w*)').findall(e)[0][1]}))}, ignore_extra_keys=True)

    create_permission_schema = Schema({
        'permission_name': str,
        'id_object': int,
        'id_action': int
    })

    modify_permission_schema = Schema({
        Optional('permission_name'): str,
        Optional('id_object'): int,
        Optional('id_action'): int
    })

    create_role_schema = Schema({
        'role_name': str,
        Optional('id_pool', default=None): Or(int, None),
        Optional('permissions'): Schema([int])
    })

    modify_role_schema = Schema({
        Optional('role_name'): str,
        Optional('id_pool'): Or(int, None),
        Optional('emp', default=[]): Schema([int]), # добавляемые разрешения
        Optional('abol', default=[]): Schema([int]) # удаляемые разрешения
    })

    modify_user_roles_schema = Schema({
        Optional('emp', default=[]): Schema([int]),
        Optional('abol', default=[]): Schema([int])
    }, ignore_extra_keys=True)

    edit_user_schema = Schema({
        Optional('name'): str,
        Optional('last_name'): str,
        Optional('email'): str,
        Optional('user_name'): str,
        Optional('password'): str,
    }, ignore_extra_keys=True)

    create_server_schema = Schema(
        {'server_name': Regex(r'\w*'), 'ip': str, 'login': Regex(r'\w*'), 'password': Regex(r'\w*')},
        ignore_extra_keys=True)

    create_user_schema = Schema({
        Optional('name'): str,
        Optional('last_name'): str,
        Optional('email'): str,
        'user_name': Regex(r'\w*'),
        'password': Regex(r'\w*'),
        Optional('emp', default=[]): Schema([int]),
    })

    connect_server_to_pool_schema = Schema({'id_pool': int}, ignore_extra_keys=True)
    create_pool_schema = Schema({'pool_name': str, 'description': str, 'domain': str}, ignore_extra_keys=True)

    post_server_schema = Schema({
        'method': Or('AD', 'connect', 'disconnect')
    }, ignore_extra_keys=True)

    set_server_ad_settings_schema = Schema({
        'ad_login': Regex(r'\w+'),
        'ad_password': str
    }, ignore_extra_keys=True)

    @staticmethod
    def sort_schema(obj):
        return Schema({
            Optional('{}_sort_by'.format(obj), default=cs.DefaultSort.order_by.format(obj)): str,
            Optional('{}_order'.format(obj), default=cs.DefaultSort.order_dir): Or('ASC', 'DESC'),
            Optional('{}_count'.format(obj), default=cs.DefaultSort.limit): Or('ALL', And(str, Use(lambda e: int(e)))),
            Optional('{}_page'.format(obj), default=cs.DefaultSort.page): And(str, Use(lambda e: int(e))),
        }, ignore_extra_keys=True)

    @staticmethod
    def validate(obj, my_schema):
        if hasattr(my_schema, 'validate') and callable(my_schema.validate):
            return my_schema.validate(obj)
        elif (type(my_schema) is list) and (type(obj) is list) and (len(obj) == len(my_schema)):
            return [APISchema.validate(obj[i], my_schema[i]) for i in range(len(obj))]

    @staticmethod
    def validate_list_to_dict(obj, my_schema):
        obj = APISchema.validate(obj, my_schema)
        return {k: v for d in obj for k, v in d.items()}


if __name__ == '__main__':
    print({k: v for d in APISchema.validate([1, 'Andrey', 'Grigoryev', 'AAGrigoryev@sbcloud.ru', 'AAGrigoryev',
                                             '1a011e415dffd0491570c642af2ea15a5990442f86812b8b4cf7e748a6341fcf', None,
                                             datetime(2020, 7, 15, 17, 19, 59, 118664)], APISchema.get_user_schema) for
           k, v in d.items()})
