import re
import json
import traceback

from Utils import *
from Exceptions import *
import Constants as cs
from Schemas import APISchema


# Все параметры в методах классов Control (Типа UserControl, RoleControl) ДОЛЖНЫ УКАЗЫВАТЬСЯ ИМЕННОВАНО !!!
# (иначе будет ошибка в декораторе RoleControl.check_role)

class APIRequest:  # Декораторы для обработки запроса

    @staticmethod
    def exec_method(journal_on_success=True, journal_on_error=True, nj_id_object=None, nj_id_action=None,
                    nj_description=None):  # Декоратор, форматирующий ответ и отлавлиающий ошибки (Ставится на функцию, возвращающую tuple из result и journal)
        def decorator(func):
            def wrapper(*args, **kwargs):
                try:
                    if not ClientControl()._client_exists(client_ip=args[0].client_ip):
                        ClientControl()._create_client(client_ip=args[0].client_ip)
                    result, journal = func(*args, **kwargs)
                    if isinstance(result, list):
                        result = {'data': result}

                    if journal_on_success:
                        id_object = journal['id_object'] if 'id_object' in journal else nj_id_object
                        id_action = journal['id_action'] if 'id_action' in journal else nj_id_action
                        description = journal['description'] if 'description' in journal else nj_description
                        id_vdi_user = journal['current_user'] if 'current_user' in journal else None
                        current_object = journal['current_object'] if 'current_object' in journal else None
                        JournalControl()._create_journal_record(id_vdi_user=id_vdi_user,
                                                                id_client=ClientControl()._get_client(
                                                                    client_ip=args[0].client_ip)['id_client'],
                                                                id_object=id_object, id_action=id_action,
                                                                current_object=current_object,
                                                                description=journal['description']
                                                                if 'description' in journal
                                                                else
                                                                (nj_description
                                                                 if nj_description is not None
                                                                 else
                                                                 'Success on {} {}'.format(
                                                                     cs.ROLE.objects.object_name[id_object - 1],
                                                                     cs.ROLE.actions.action_name[id_action - 1]
                                                                 )))
                    return Response.success(**result), cs.StatusCodes.ok_code
                except JournaledException as m:
                    trace = traceback.format_exc()
                    if journal_on_error:
                        journal = m.data
                        id_object = journal['id_object'] if 'id_object' in journal else nj_id_object
                        id_action = journal['id_action'] if 'id_action' in journal else nj_id_action
                        id_vdi_user = journal['current_user'] if 'current_user' in journal else None
                        current_object = journal['current_object'] if 'current_object' in journal else None
                        JournalControl()._create_journal_record(id_vdi_user=id_vdi_user,
                                                                id_client=ClientControl()._get_client(
                                                                    client_ip=args[0].client_ip)['id_client'],
                                                                id_object=id_object, id_action=id_action,
                                                                current_object=current_object,
                                                                success=False,
                                                                description=str(m).replace('\'', ''))
                    print(trace)
                    return Response.error(m.code, str(m), trace=trace), m.code
                except Exception as e:
                    trace = traceback.format_exc()
                    if journal_on_error:
                        JournalControl()._create_journal_record(id_client=ClientControl()._get_client(
                            client_ip=args[0].client_ip)['id_client'],
                                                                success=False,
                                                                description='Unknown error:\n' + str(e).replace('\'', ''))
                    print(trace)
                    return Response.error(cs.StatusCodes.internal_server_error_code,
                                          str(e), trace=trace), cs.StatusCodes.internal_server_error_code

            return wrapper

        return decorator

    @staticmethod
    def check_data_schema(schema):  # Декоратор для проверки схемы JSON-запроса (сует в kwargs поле valid_data)
        def decorator(func):
            def wrapper(*args, **kwargs):
                data = args[0].data
                try:
                    data = json.loads(data.decode())
                except Exception:
                    data = json.loads(data)
                kwargs['valid_data'] = APISchema.validate(data, schema)
                return func(*args, **kwargs)

            return wrapper

        return decorator

    @staticmethod
    def check_headers_schema(schema):  # Декоратор для проверки схемы заголовков (сует в kwargs поле valid_headers)
        def decorator(func):
            def wrapper(*args, **kwargs):
                kwargs['valid_headers'] = APISchema.validate(args[0].headers, schema)
                return func(*args, **kwargs)

            return wrapper

        return decorator

    @staticmethod
    def check_auth(func):  # Декоратор, ищущий acces_token в куках и сующий его в параметр auth_token
        def wrapper(*args, **kwargs):
            c_dict = APIRequest.parse_cookie(args[0].headers)
            if 'auth_token' not in c_dict:
                raise AuthException('Unauthorized request')
            kwargs['auth_token'] = c_dict['auth_token']
            return func(*args, **kwargs)

        return wrapper

    @staticmethod
    def sorted(obj):  # Декоратор, подставляющий в метод параметры сортировки из куков
        def decorator(func):
            def wrapper(*args, **kwargs):
                cookie = APIRequest.parse_cookie(args[0].headers)
                print(cookie['journal_page']) if 'journal_page' in cookie else None
                sort_params = APISchema.validate(cookie, APISchema.sort_schema(obj))
                kwargs['order_by'] = sort_params['{}_sort_by'.format(obj)]
                kwargs['order_dir'] = sort_params['{}_order'.format(obj)]
                kwargs['limit'] = sort_params['{}_count'.format(obj)]
                kwargs['offset'] = 0 if sort_params[
                                            '{}_count'.format(obj)
                                        ] == cs.DefaultSort.limit else sort_params['{}_page'.format(obj)] * sort_params[
                    '{}_count'.format(obj)]
                return func(*args, **kwargs)

            return wrapper

        return decorator

    @staticmethod
    def parse_cookie(headers):

        if 'Cookie' not in headers:
            if 'Authorization' not in headers:
                return {}
        cookie = headers['Authorization'] if 'Cookie' not in headers else headers['Cookie']
        cookie_parser = re.compile(r'((| )\S(\S*)=\S(\S*)((; )|))')
        parsed = [v[0].replace(' ', '').replace(';', '') for v in cookie_parser.findall(cookie)]
        pairs = [pair.split('=', 1) for pair in parsed]
        return {k: v for k, v in pairs}

    # Все параметры в методах классов Control (Типа UserControl, RoleControl) ДОЛЖНЫ УКАЗЫВАТЬСЯ ИМЕННОВАНО !!!
    # (иначе будет ошибка в декораторе RoleControl.check_role)


class API:

    def __init__(self, client_ip, data=None, headers=None):
        self.client_ip = client_ip
        self.data = data
        self.headers = headers

    def general(self, name, sub=None):
        if name not in ['sign_in', 'sign_out', 'servers', 'sessions', 'pool', 'roles']:
            return Response.error(cs.StatusCodes.not_found_code,
                                  '{}: Method "{}" not found'.format(
                                      cs.StatusCodes.not_found_code, name)), cs.StatusCodes.not_found_code

    @APIRequest.exec_method(journal_on_success=False)
    @APIRequest.check_auth
    @APIRequest.sorted('journal')
    def get_journal_records(self, auth_token=None, order_by=cs.DefaultSort.order_by.format('journal_record'),
                            order_dir=cs.DefaultSort.order_dir,
                            limit=cs.DefaultSort.limit, offset=cs.DefaultSort.offset, *args, **kwargs):
        result, journal = JournalControl(id_token=auth_token).get_journal_record_list(order_by=order_by, order_dir=order_dir, limit=limit, offset=offset)
        return {
                   'data': result,
                   'count': len(JournalControl()._get_journal_record_list())
               }, journal

    @APIRequest.exec_method()
    @APIRequest.check_headers_schema(APISchema.sign_in_schema)
    def sign_in(self, valid_headers=None, *args, **kwargs):
        user_name, hashed_pass = valid_headers['Authorization']['user_name'], valid_headers['Authorization'][
            'hashed_pass']
        return UserControl().sign_in(user_name=user_name, hashed_pass=hashed_pass)

    @APIRequest.exec_method()
    @APIRequest.check_auth
    def sign_out(self, auth_token=None):
        return UserControl(id_token=auth_token).sign_out()

    @APIRequest.exec_method(journal_on_success=False)
    @APIRequest.check_auth
    @APIRequest.sorted('permission')
    def get_permission_list(self, auth_token=None, order_by=cs.DefaultSort.order_by.format('permission'),
                            order_dir=cs.DefaultSort.order_dir,
                            limit=cs.DefaultSort.limit, offset=cs.DefaultSort.offset, *args, **kwargs):
        control = PermissionControl(id_token=auth_token)
        data, journal = control.get_permission_list(order_by=order_by, order_dir=order_dir, limit=limit, offset=offset)
        return {'data': data,
                'count': control._count()}, journal

    @APIRequest.exec_method(journal_on_success=False)
    @APIRequest.check_auth
    def get_permission_data(self, id_permission, auth_token=None, *args, **kwargs):
        return PermissionControl(id_token=auth_token).get_permission_data(id_permission=id_permission)

    @APIRequest.exec_method(journal_on_success=False)
    @APIRequest.check_auth
    @APIRequest.check_data_schema(APISchema.create_permission_schema)
    def create_permission(self, auth_token=None, valid_data=None, *args, **kwargs):
        return PermissionControl(id_token=auth_token).create_permission(permission_name=valid_data['permission_name'],
                                                                        id_object=valid_data['id_object'],
                                                                        id_action=valid_data['id_action'])

    @APIRequest.exec_method(journal_on_success=False)
    @APIRequest.check_auth
    @APIRequest.check_data_schema(APISchema.modify_permission_schema)
    def edit_permission(self, id_permission, auth_token=None, valid_data=None, *args, **kwargs):
        return PermissionControl(id_token=auth_token).modify_permission(id_permission=id_permission, **valid_data)

    @APIRequest.exec_method(journal_on_success=False)
    @APIRequest.check_auth
    def delete_permission(self, id_permission, auth_token=None, *args, **kwargs):
        return PermissionControl(id_token=auth_token).delete_permission(id_permission=id_permission)

    @APIRequest.exec_method(journal_on_success=False)
    @APIRequest.check_auth
    @APIRequest.sorted('role')
    def get_role_list(self, auth_token=None, order_by=cs.DefaultSort.order_by.format('role'),
                      order_dir=cs.DefaultSort.order_dir,
                      limit=cs.DefaultSort.limit, offset=cs.DefaultSort.offset, *args, **kwargs):
        control = RoleControl(id_token=auth_token)
        data, journal = control.get_role_list(order_by=order_by, order_dir=order_dir, limit=limit, offset=offset)
        return {'data': data,
                'count': control._count()}, journal

    @APIRequest.exec_method(journal_on_success=False)
    @APIRequest.check_auth
    def get_role_data(self, id_role=None, auth_token=None, *args, **kwargs):
        return RoleControl(id_token=auth_token).get_role_data(id_role=id_role)

    @APIRequest.exec_method(journal_on_success=False)
    @APIRequest.check_auth
    @APIRequest.check_data_schema(APISchema.create_role_schema)
    def create_role(self, auth_token=None, valid_data=None, *args, **kwargs):
        return RoleControl(id_token=auth_token).create_role(role_name=valid_data['role_name'],
                                                            id_pool=valid_data['id_pool'],
                                                            permissions=valid_data['permissions'])

    @APIRequest.exec_method(journal_on_success=False)
    @APIRequest.check_auth
    @APIRequest.check_data_schema(APISchema.modify_role_schema)
    def edit_role(self, id_role, auth_token=None, valid_data=None, *args, **kwargs):
        return RoleControl(id_token=auth_token).modify_role(id_role=id_role, **valid_data)

    @APIRequest.exec_method(journal_on_success=False)
    @APIRequest.check_auth
    def delete_role(self, id_role, auth_token, *args, **kwargs):
        return RoleControl(id_token=auth_token).delete_role(id_role=id_role)

    @APIRequest.exec_method(journal_on_success=False)
    @APIRequest.check_auth
    def my_data(self, auth_token=None, *args, **kwargs):
        return UserControl(id_token=auth_token).get_my_data()

    @APIRequest.exec_method(journal_on_success=False)
    @APIRequest.check_auth
    @APIRequest.sorted('vdi_user')
    def get_user_list(self, auth_token=None, order_by=cs.DefaultSort.order_by, order_dir=cs.DefaultSort.order_dir,
                      limit=cs.DefaultSort.limit, offset=cs.DefaultSort.offset, *args, **kwargs):
        control = UserControl(id_token=auth_token)
        result, journal = control.get_user_list(order_by=order_by, order_dir=order_dir, limit=limit, offset=offset)
        return {
                   'data': result,
                   'count': control._count()
               }, journal

    @APIRequest.exec_method()
    @APIRequest.check_auth
    def get_user_data(self, id_vdi_user, auth_token=None, *args, **kwargs):
        return UserControl(id_token=auth_token).get_user_data(id_vdi_user=id_vdi_user)

    @APIRequest.exec_method()
    @APIRequest.check_auth
    @APIRequest.check_data_schema(APISchema.create_user_schema)
    def create_user(self, auth_token=None, valid_data=None, *args, **kwargs):
        return UserControl(id_token=auth_token).create_user(**valid_data)

    @APIRequest.exec_method()
    @APIRequest.check_auth
    @APIRequest.check_data_schema(APISchema.edit_user_schema)
    def edit_user(self, id_vdi_user, auth_token=None, valid_data=None, *args, **kwargs):
        UserControl(id_token=auth_token).modify_user(id_vdi_user=id_vdi_user, **valid_data)
        return self.modify_user_roles(id_vdi_user=id_vdi_user, auth_token=auth_token)

    @APIRequest.check_data_schema(APISchema.modify_user_roles_schema)
    def modify_user_roles(self, id_vdi_user, auth_token=None, valid_data=None, *args, **kwargs):
        for role in valid_data['emp']:
            UserControl(id_token=auth_token).add_user_role(id_vdi_user=id_vdi_user, id_role=role)
        for role in valid_data['abol']:
            UserControl(id_token=auth_token).delete_user_role(id_vdi_user=id_vdi_user, id_role=role)
        return UserControl(id_token=auth_token)._get_user_data(id_vdi_user=id_vdi_user), {
            'id_object': cs.ROLE.objects.user, 'id_action': cs.ROLE.objects.U
        }

    @APIRequest.exec_method()
    @APIRequest.check_auth
    def delete_user(self, id_vdi_user, auth_token=None, *args, **kwargs):
        return UserControl(id_token=auth_token).delete_user(id_vdi_user=id_vdi_user)

    @APIRequest.exec_method(journal_on_success=False)
    @APIRequest.check_auth
    @APIRequest.sorted('server')
    def get_server_list(self, auth_token=None, order_by=cs.DefaultSort.order_by, order_dir=cs.DefaultSort.order_dir,
                        limit=cs.DefaultSort.limit, offset=cs.DefaultSort.offset, *args, **kwargs):
        control = ServerControl(id_token=auth_token)
        data, journal = control.get_server_list(order_by=order_by, order_dir=order_dir, limit=limit, offset=offset)
        return {'data': data,
                'count': control._count()}, journal

    @APIRequest.exec_method()
    @APIRequest.check_auth
    def get_server_data(self, id_server: int, auth_token=None, *args, **kwargs):
        return ServerControl(id_token=auth_token).get_server_data(id_server=id_server)

    @APIRequest.exec_method()
    @APIRequest.check_auth
    def delete_server(self, id_server, auth_token=None, *args, **kwargs):
        return ServerControl(id_token=auth_token).delete_server(id_server=id_server)

    @APIRequest.exec_method()
    @APIRequest.check_auth
    @APIRequest.check_data_schema(APISchema.create_server_schema)
    def create_server(self, auth_token=None, valid_data=None, *args, **kwargs):
        return ServerControl(id_token=auth_token).create_server(server_name=valid_data['server_name'],
                                                                ip=valid_data['ip'],
                                                                login=valid_data['login'],
                                                                password=valid_data['password'])

    @APIRequest.exec_method()
    @APIRequest.check_auth
    @APIRequest.check_data_schema(APISchema.post_server_schema)
    def post_server(self, id_server, valid_data=None, auth_token=None, *args, **kwargs):
        if valid_data['method'] == 'AD':
            return self.customize_server(id_server=id_server, auth_token=auth_token)
        elif valid_data['method'] == 'connect':
            return self.connect_server_to_pool(id_server=id_server, auth_token=auth_token)
        elif valid_data['method'] == 'disconnect':
            return self.disconnect_server_from_pool(id_server=id_server, auth_token=auth_token)

    @APIRequest.check_data_schema(APISchema.set_server_ad_settings_schema)
    def customize_server(self, id_server, auth_token=None, valid_data=None, *args, **kwargs):
        return ServerControl(id_token=auth_token).set_server_ad_settings(id_server, valid_data['ad_login'],
                                                                         valid_data['ad_password'])

    @APIRequest.check_data_schema(APISchema.connect_server_to_pool_schema)
    def connect_server_to_pool(self, id_server, auth_token=None, valid_data=None, *args, **kwargs):
        return PoolControl(id_token=auth_token).connect_server_to_pool(id_server=id_server,
                                                                       id_pool=valid_data['id_pool'])

    def disconnect_server_from_pool(self, id_server, auth_token=None, *args, **kwargs):
        return ServerControl(id_token=auth_token).disconnect_server_from_pool(id_server=id_server)

    @APIRequest.exec_method(journal_on_success=False)
    @APIRequest.check_auth
    @APIRequest.sorted('session')
    def get_sessions_list(self, auth_token=None, order_by=cs.DefaultSort.order_by, order_dir=cs.DefaultSort.order_dir,
                          limit=cs.DefaultSort.limit, offset=cs.DefaultSort.offset, *args, **kwargs):
        control = SessionControl(id_token=auth_token)
        data, journal = control.get_session_list(order_by=order_by, order_dir=order_dir, limit=limit, offset=offset)
        return {'data': data,
                'count': control._count()}, journal

    @APIRequest.exec_method()
    @APIRequest.check_auth
    def get_session_data(self, id_session: str, auth_token=None, *args, **kwargs):
        return SessionControl(id_token=auth_token).get_session_data(id_session=id_session)

    @APIRequest.exec_method()
    @APIRequest.check_auth
    def close_session(self, id_session: str, auth_token=None, *args, **kwargs):
        return SessionControl(id_token=auth_token).close_session(id_session=id_session)

    @APIRequest.exec_method()
    @APIRequest.check_auth
    def block_session(self, id_session: str, auth_token=None, *args, **kwargs):
        return SessionControl(id_token=auth_token).block_session(id_session=id_session)

    @APIRequest.exec_method()
    @APIRequest.check_auth
    def unblock_session(self, id_session: str, auth_token=None, *args, **kwargs):
        return SessionControl(id_token=auth_token).unblock_session(id_session=id_session)

    @APIRequest.exec_method(journal_on_success=False)
    @APIRequest.check_auth
    @APIRequest.sorted('pool')
    def get_pool_list(self, auth_token=None, order_by=cs.DefaultSort.order_by, order_dir=cs.DefaultSort.order_dir,
                      limit=cs.DefaultSort.limit, offset=cs.DefaultSort.offset, *args, **kwargs):
        control = PoolControl(id_token=auth_token)
        data, journal = control.get_pool_list(order_by=order_by, order_dir=order_dir, limit=limit, offset=offset)
        return {'data': data,
                'count': control._count()}, journal

    @APIRequest.exec_method()
    @APIRequest.check_auth
    def get_pool_data(self, id_pool, auth_token=None, *args, **kwargs):
        return PoolControl(id_token=auth_token).get_pool_data(id_pool=id_pool)

    @APIRequest.exec_method()
    @APIRequest.check_auth
    @APIRequest.check_data_schema(APISchema.create_pool_schema)
    def create_pool(self, auth_token=None, valid_data=None, *args, **kwargs):
        return PoolControl(id_token=auth_token).create_pool(pool_name=valid_data['pool_name'],
                                                            description=valid_data['description'],
                                                            domain=valid_data['domain'])

    @APIRequest.exec_method()
    @APIRequest.check_auth
    def connect_pool_to_broker(self, id_pool, auth_token=None, *args, **kwargs):
        return PoolControl(id_token=auth_token).connect_pool_to_broker(id_pool=id_pool)

    @APIRequest.exec_method()
    @APIRequest.check_auth
    def disconnect_pool_from_broker(self, id_pool, auth_token=None, *args, **kwargs):
        return PoolControl(id_token=auth_token).disconnect_pool_from_broker(id_pool=id_pool)

    @APIRequest.exec_method()
    @APIRequest.check_auth
    def delete_pool(self, id_pool, auth_token=None, *args, **kwargs):
        return PoolControl(id_token=auth_token).delete_pool(id_pool=id_pool)

    @staticmethod
    def after_request(response):
        response.headers.add('Access-Control-Allow-Origin', 'http://185.17.141.230')  # http://95.128.181.113:5000
        response.headers.add('Access-Control-Allow-Headers', 'Content-Type,Authorization,Cookie')
        response.headers.add('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE,OPTIONS,PATCH')
        response.headers.add('Access-Control-Allow-Credentials', 'true')
        return response
