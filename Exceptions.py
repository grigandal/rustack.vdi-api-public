import Constants as cs


class MethodException(Exception):
    def __init__(self, msg: str, code=cs.StatusCodes.internal_server_error_code, *args, **kwargs):
        self.code = code
        self.message = msg
        super().__init__(msg, *args)

    def __str__(self):
        return '{}: {}'.format(self.code, super().__str__())


class JournaledException(MethodException):
    def __init__(self, msg: str, code=cs.StatusCodes.internal_server_error_code, *args, **kwargs):
        super().__init__(msg=msg, code=code, *args, **kwargs)
        self.data = kwargs

    @staticmethod
    def from_exception(m: Exception, *args, **kwargs):
        if isinstance(m, JournaledException):
            return JournaledException(msg=m.message, code=m.code, **m.data)
        elif isinstance(m, MethodException):
            return JournaledException(msg=m.message, code=m.code, *args, **kwargs)
        else:
            return JournaledException(str(m),
                                      code=kwargs['code']
                                      if 'code' in kwargs
                                      else cs.StatusCodes.internal_server_error_code,
                                      *args,
                                      **kwargs)


class AfterAuthException(JournaledException):
    def __init__(self, msg, code=cs.StatusCodes.internal_server_error_code, current_user=None, *args, **kwargs):
        super().__init__(msg=msg, code=code, current_user=current_user)

    @property
    def current_user(self):
        return self.data['current_user']


class AccessException(JournaledException):
    def __init__(self, msg='Empowerment required', code=cs.StatusCodes.forbidden_code, id_object=None, id_action=None, current_object=None, *args, **kwargs):
        super().__init__(msg=msg, code=code, id_object=id_object, id_action=id_action,
                         current_object=current_object)

    @property
    def id_object(self):
        return self.data['id_object']

    @property
    def id_action(self):
        return self.data['id_action']


class ClientException(MethodException):
    pass


class JournalException(MethodException):
    pass


class UserException(MethodException):
    pass


class UserCreateException(UserException):
    pass


class AuthException(AccessException):
    def __init__(self, msg: str, code=cs.StatusCodes.unauthorized_code):
        super().__init__(msg,
                         code=code,
                         id_object=cs.ROLE.objects.user,
                         id_action=cs.ROLE.actions.auth)


class PermissionException(MethodException):
    pass


class RoleException(MethodException):
    pass


class ServerException(MethodException):
    pass


class ServerCreateException(ServerException):
    pass


class SessionException(MethodException):
    pass


class AbortSessionException(SessionException):
    pass


class PoolException(MethodException):
    pass


class PoolCreateException(PoolException):
    pass


class FindSQLError(MethodException):
    pass
