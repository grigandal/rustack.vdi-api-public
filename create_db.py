from Utils import DBBroker
import Constants as cs

if __name__ == '__main__':
    connection = DBBroker.get_connection(cs.Config.base_config)
    sql_text = open('sql/create_db.sql').read()
    DBBroker.send_sql(sql_text, connection)
    connection.commit()
    connection.close()