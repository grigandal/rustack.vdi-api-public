import base64
from uuid import uuid4
import hashlib
from psycopg2 import connect, extensions, sql
import re
import pandas as pd
from datetime import datetime

import Constants as cs
from Schemas import APISchema
from Exceptions import *
import VDI


class Response:
    @staticmethod
    def success(**kwargs):
        return {'success': True, 'result': kwargs, 'error': None}

    @staticmethod
    def error(code: int, desc: str, trace=None) -> object:
        return {'success': False, 'result': None, 'error': {
            'code': code, 'desc': desc, 'trace': None if trace is None else Utils.str_to_base64(str(trace))
        }}


class Utils:
    @staticmethod
    def generate_token(*args, **kwargs):
        return base64.b64encode(str(uuid4()).encode()).decode()

    @staticmethod
    def str_to_base64(s):
        return base64.b64encode(s.encode()).decode()

    @staticmethod
    def base64_to_str(s):
        return base64.b64decode(s.encode()).decode()

    @staticmethod
    def hash(s: str, h_type='sha256'):
        if h_type == 'md5':
            return hashlib.md5(s.encode()).hexdigest()
        elif h_type == 'sha256':
            return hashlib.sha256(s.encode('utf-8')).hexdigest()

    @staticmethod
    def group_role_list(role_list):
        if len(role_list) == 0:
            return []
        result = pd.DataFrame(role_list).fillna('platform').astype(int, errors='ignore').groupby([
            'id_role', 'role_name', 'id_pool', 'pool_name', 'id_role_type'
        ])[['id_permission', 'permission_name', 'id_object', 'id_action']].apply(
            lambda e: e.to_dict('r')
        ).reset_index(name='permissions').to_dict('r')

        return result

    @staticmethod
    def group_user_role_list(user_role_list):
        if len(user_role_list) == 0:
            return []
        result = pd.DataFrame(user_role_list).fillna('platform').astype(int, errors='ignore').groupby([
            'id_pool', 'pool_name'
        ])[['id_role', 'role_name']].apply(
            lambda e: e.to_dict('r')
        ).reset_index(name='roles').to_dict('r')
        return result

    @staticmethod
    def group_permissions(permissions):
        if len(permissions) == 0:
            return []
        result = pd.DataFrame(permissions).astype(int, errors='ignore').groupby([
            'id_object'
        ])[['id_action']].apply(
            lambda e: [a['id_action'] for a in e.to_dict('r')]
        ).reset_index(name='actions').to_dict('r')
        return result

    @staticmethod
    def group_user_permission_list(user_permission_list):
        if len(user_permission_list) == 0:
            return []
        result = \
            pd.DataFrame(user_permission_list).fillna('platform').astype(int, errors='ignore').groupby(['id_pool'])[[
                'id_object', 'id_action']
            ].apply(
                lambda e: e.to_dict('r')
            ).reset_index(name='permissions').to_dict('r')
        for pool_permission in result:
            result[result.index(pool_permission)]['permissions'] = Utils.group_permissions(
                pool_permission['permissions'])
        return result

    @staticmethod
    def check_null(v, do_is=False):
        t = '{}'
        if type(v) == str:
            t = '\'{}\''
        return (('= ' + t) if do_is else t).format(v) if v is not None else '{}NULL'.format(
            ' is ' if do_is else '')

    @staticmethod
    def user_is_current(*args, **kwargs):
        return ('current_user' in kwargs) and (kwargs['current_user'] == kwargs['id_vdi_user'])


class DBBroker:
    @staticmethod
    def connect(host=None, dbname=None, user=None, password=None, *args, **kwargs):
        if (host is not None) & (dbname is not None) & (user is not None) & (password is not None):
            return connect(host=host, dbname=dbname, user=user, password=password)
        return None

    @staticmethod
    def read_config(path: str):
        with open(path, 'r') as cfg:
            pairs = [pair.split('=') for pair in cfg.read().split('\n')]

            for i in range(len(pairs) - 1, -1, -1):
                if len(pairs[i]) != 2:
                    pairs.remove(pairs[i])

            return {
                key: value
                for key, value in pairs
            }

    @staticmethod
    def get_connection(config_path: str):
        connection = DBBroker.connect(**DBBroker.read_config(config_path))
        return connection

    @staticmethod
    def send_sql(sql_text: str, connection):
        cursor = connection.cursor()
        cursor.execute(sql.SQL(sql_text))
        return cursor

    @staticmethod
    def send_select(select_text: str, connection):
        cursor = DBBroker.send_sql(select_text, connection)
        return cursor.fetchall()


class DBControl:
    def __init__(self, config_path=cs.Config.base_config, id_token=None):
        self.config_path = config_path
        self.id_token = id_token


class AuthControl(DBControl):

    @staticmethod
    def search_token(id_token, config_path=cs.Config.base_config):
        connection = DBBroker.get_connection(config_path)
        sql_text = cs.SQL.search_token.format(id_token)
        result = DBBroker.send_select(sql_text, connection)
        connection.close()
        if len(result) == 0:
            raise AuthException('Unauthorized request')
        return APISchema.validate_list_to_dict(list(result[0]), APISchema.search_token_schema)

    @staticmethod
    def authorized(method):
        def wrapper(*args, **kwargs):
            token = AuthControl.search_token(args[0].id_token, args[0].config_path)
            kwargs['current_user'] = current_user = token['id_vdi_user']
            try:
                result = method(*args, **kwargs)
                journal = dict()
                if len(result) == 2 and isinstance(result, tuple):
                    journal = result[1]
                    result = result[0]
                journal['current_user'] = current_user
                return result, journal
            except Exception as e:
                raise AfterAuthException.from_exception(e, current_user=current_user)

        return wrapper


class PermissionControl(AuthControl):
    def _get_permission(self, id_permission, *args, **kwargs):
        connection = DBBroker.get_connection(self.config_path)
        sql_text = cs.SQL.get_permission.format(id_permission)
        result = DBBroker.send_select(sql_text, connection)
        connection.close()
        if len(result) == 0:
            raise PermissionException('Permission with id = {} not found'.format(id_permission),
                                      code=cs.StatusCodes.not_found_code)
        if len(result) > 1:
            raise PermissionException('Found more than one permission with id = {}'.format(id_permission))
        return APISchema.validate_list_to_dict(list(result[0]), APISchema.get_permission_schema)

    def _permission_exists(self, id_permission, *args, **kwargs):
        try:
            self._get_permission(id_permission=id_permission)
        except PermissionException as e:
            if e.code == cs.StatusCodes.not_found_code:
                return False
            raise e
        return True

    def _get_permission_data(self, id_permission, *args, **kwargs):
        if not self._permission_exists(id_permission=id_permission):
            raise PermissionException('Permission with id = {} not found'.format(id_permission),
                                      code=cs.StatusCodes.not_found_code)

        return self._get_permission(id_permission=id_permission)

    def _get_permission_list(self, order_by=cs.DefaultSort.order_by.format('permission'),
                             order_dir=cs.DefaultSort.order_dir,
                             limit=cs.DefaultSort.limit, offset=cs.DefaultSort.offset, *args, **kwargs):
        connection = DBBroker.get_connection(self.config_path)
        sql_text = cs.SQL.get_permission_list.format(order_by, order_dir, limit, offset)
        result = DBBroker.send_select(sql_text, connection)
        connection.close()
        return [APISchema.validate_list_to_dict(list(r), APISchema.get_permission_schema) for r in result]

    def _get_user_permission_list(self, id_vdi_user, *args, **kwargs):
        if not UserControl()._user_exists(id_vdi_user=id_vdi_user):
            raise PermissionException('User with id = {} not found'.format(id_vdi_user),
                                      code=cs.StatusCodes.not_found_code)
        connection = DBBroker.get_connection(self.config_path)
        sql_text = cs.SQL.get_user_permission_list.format(id_vdi_user)
        result = DBBroker.send_select(sql_text, connection)
        connection.close()
        return [APISchema.validate_list_to_dict(list(r), APISchema.get_user_permission_list_schema) for r in result]

    def _get_user_permission_list_grouped(self, id_vdi_user, *args, **kwargs):
        user_permission_list = self._get_user_permission_list(id_vdi_user=id_vdi_user)
        return Utils.group_user_permission_list(user_permission_list)

    @AuthControl.authorized
    def get_permission_data(self, id_permission, *args, **kwargs):
        return self._get_permission_data(id_permission=id_permission, *args, **kwargs)

    @AuthControl.authorized
    def get_permission_list(self, order_by=cs.DefaultSort.order_by.format('permission'),
                            order_dir=cs.DefaultSort.order_dir,
                            limit=cs.DefaultSort.limit, offset=cs.DefaultSort.offset, *args, **kwargs):
        return self._get_permission_list(order_by=order_by, order_dir=order_dir, limit=limit, offset=offset)

    def _get_permission_by_OnA(self, id_object, id_action, *args, **kwargs):
        connection = DBBroker.get_connection(self.config_path)
        sql_text = cs.SQL.get_permission_by_OnA.format(id_object, id_action)
        result = DBBroker.send_select(sql_text, connection)
        connection.close()
        if len(result) == 0:
            raise PermissionException(
                'Permission for object with id = {} and action with id = {} not found'.format(id_object, id_action),
                code=cs.StatusCodes.not_found_code)
        if len(result) > 1:
            raise PermissionException(
                'Found more than one permission for object with id = {} and action with id = {} not found'.format(
                    id_object, id_action))
        return APISchema.validate_list_to_dict(list(result[0]), APISchema.get_permission_schema)

    def _permission_exists_OnA(self, id_object, id_action, *args, **kwargs):
        try:
            self._get_permission_by_OnA(id_object=id_object, id_action=id_action)
        except PermissionException as e:
            if e.code == cs.StatusCodes.not_found_code:
                return False
            raise e
        return True

    def _create_permission(self, permission_name, id_object, id_action, *args, **kwargs):
        if self._permission_exists_OnA(id_object=id_object, id_action=id_action):
            return self._get_permission_by_OnA(id_object=id_object, id_action=id_action)

        connection = DBBroker.get_connection(self.config_path)
        sql_text = cs.SQL.create_permission.format(permission_name, id_object, id_action)
        DBBroker.send_sql(sql_text, connection)
        connection.commit()
        connection.close()
        return self._get_permission_by_OnA(id_object=id_object, id_action=id_action)

    def _delete_permission(self, id_permission, *args, **kwargs):
        connection = DBBroker.get_connection(self.config_path)
        sql_text = cs.SQL.delete_permission.format(id_permission)
        DBBroker.send_sql(sql_text, connection)
        connection.commit()
        connection.close()
        return {}

    def _delete_permission_OnA(self, id_object, id_action, *args, **kwargs):
        connection = DBBroker.get_connection(self.config_path)
        sql_text = cs.SQL.create_permission_OnA.format(id_object=id_object, id_action=id_action)
        DBBroker.send_sql(sql_text, connection)
        connection.commit()
        connection.close()
        return {}

    def _set_permission_object(self, id_permission, id_object, *args, **kwargs):
        connection = DBBroker.get_connection(self.config_path)
        sql_text = cs.SQL.set_permission_object.format(id_object, id_permission)
        DBBroker.send_sql(sql_text, connection)
        connection.commit()
        connection.close()
        return self._get_permission_data(id_permission=id_permission)

    def _set_permission_action(self, id_permission, id_action, *args, **kwargs):
        connection = DBBroker.get_connection(self.config_path)
        sql_text = cs.SQL.set_permission_action.format(id_action, id_permission)
        DBBroker.send_sql(sql_text, connection)
        connection.commit()
        connection.close()
        return self._get_permission_data(id_permission=id_permission)

    def _set_permission_name(self, id_permission, permission_name, *args, **kwargs):
        connection = DBBroker.get_connection(self.config_path)
        sql_text = cs.SQL.set_permission_name.format(permission_name, id_permission)
        DBBroker.send_sql(sql_text, connection)
        connection.commit()
        connection.close()
        return self._get_permission_data(id_permission=id_permission)

    def _modify_permission(self, id_permission, *args, **kwargs):
        self._get_permission(id_permission=id_permission)
        if 'id_object' in kwargs:
            self._set_permission_object(id_permission, kwargs['id_object'])
        if 'id_action' in kwargs:
            self._set_permission_action(id_permission, kwargs['id_action'])
        if 'permission_name' in kwargs:
            self._set_permission_name(id_permission, kwargs['permission_name'])
        return self._get_permission_data(id_permission=id_permission)

    @AuthControl.authorized
    def create_permission(self, permission_name, id_object, id_action, *args, **kwargs):
        return self._create_permission(permission_name=permission_name, id_object=id_object, id_action=id_action, *args,
                                       **kwargs)

    @AuthControl.authorized
    def modify_permission(self, id_permission, *args, **kwargs):
        return self._modify_permission(id_permission=id_permission, *args, **kwargs)

    @AuthControl.authorized
    def delete_permission(self, id_permission, *args, **kwargs):
        return self._delete_permission(id_permission=id_permission, *args, **kwargs)

    def _count(self):
        return len(self._get_permission_list())


class RoleControl(PermissionControl):

    def _get_role_template(self, id_role_template, *args, **kwargs):
        connection = DBBroker.get_connection(self.config_path)
        sql_text = cs.SQL.get_role_template.format(id_role_template)
        result = DBBroker.send_select(sql_text, connection)
        connection.close()
        if len(result) == 0:
            raise RoleException('Role template with id = {} not found'.format(id_role_template),
                                code=cs.StatusCodes.not_found_code)
        if len(result) > 1:
            raise RoleException('Found more than one role template with id = {}'.format(id_role_template))
        return APISchema.validate_list_to_dict(list(result[0]), APISchema.get_role_template_schema)

    def _role_template_exists(self, id_role_template, *args, **kwargs):
        try:
            self._get_role_template(id_role_template=id_role_template)
        except RoleException as e:
            if e.code == cs.StatusCodes.not_found_code:
                return False
            raise e
        return True

    def _get_role_template_list(self, *args, **kwargs):
        connection = DBBroker.get_connection(self.config_path)
        sql_text = cs.SQL.get_role_template_list
        result = DBBroker.send_select(sql_text, connection)
        connection.close()
        return [APISchema.validate_list_to_dict(list(r), APISchema.get_role_template_schema) for r in result]

    def _get_free_id_role_template(self, *args, **kwargs):
        free_id = 1
        while free_id in [t['id_role_template'] for t in self._get_role_template_list()]:
            free_id += 1
        return free_id

    def _create_role_template(self, id_role_type, *args, **kwargs):
        if id_role_type not in [1, 2]:
            raise RoleException('Role type must be 1 (platform) or 2 (pool)', code=cs.StatusCodes.bad_request_code)
        id_role_template = self._get_free_id_role_template()
        connection = DBBroker.get_connection(self.config_path)
        sql_text = cs.SQL.create_role_template.format(id_role_template, id_role_type)
        DBBroker.send_sql(sql_text, connection)
        connection.commit()
        connection.close()
        return self._get_role_template(id_role_template=id_role_template)

    def _delete_role_template(self, id_role_template, *args, **kwargs):
        connection = DBBroker.get_connection(self.config_path)
        sql_text = cs.SQL.delete_role_template.format(id_role_template)
        DBBroker.send_sql(sql_text, connection)
        connection.commit()
        connection.close()
        return {}

    def _get_role_permission(self, id_role_template, id_permission):
        connection = DBBroker.get_connection(self.config_path)
        sql_text = cs.SQL.get_role_permission.format(id_role_template, id_permission)
        result = DBBroker.send_select(sql_text, connection)
        connection.close()
        if len(result) == 0:
            raise RoleException('Role permission with id = {} not found'.format(id_role_template),
                                code=cs.StatusCodes.not_found_code)
        if len(result) > 1:
            raise RoleException('Found more than one role permission with id = {}'.format(id_role_template))
        return APISchema.validate_list_to_dict(list(result[0]), APISchema.get_role_permission_schema)

    def _role_permission_exists(self, id_role_template, id_permission):
        try:
            self._get_role_permission(id_role_template=id_role_template, id_permission=id_permission)
        except RoleException as e:
            if e.code == cs.StatusCodes.not_found_code:
                return False
            raise e
        return True

    def _create_role_permission(self, id_role_template, id_permission, *args, **kwargs):
        permission = PermissionControl()._get_permission(id_permission=id_permission)
        role_template = self._get_role_template(id_role_template=id_role_template)

        if self._role_permission_exists(id_role_template=id_role_template, id_permission=id_permission):
            return self._get_role_permission(id_role_template=id_role_template, id_permission=id_permission)

        connection = DBBroker.get_connection(self.config_path)
        sql_text = cs.SQL.create_role_permission.format(id_role_template, id_permission)
        DBBroker.send_sql(sql_text, connection)
        connection.commit()
        connection.close()
        return self._get_role_permission(id_role_template=id_role_template, id_permission=id_permission)

    def _delete_role_permission(self, id_role_template, id_permission, *args, **kwargs):
        permission = PermissionControl()._get_permission(id_permission=id_permission)
        role_template = self._get_role_template(id_role_template=id_role_template)

        if not self._role_permission_exists(id_role_template=id_role_template, id_permission=id_permission):
            return {}

        connection = DBBroker.get_connection(self.config_path)
        sql_text = cs.SQL.delete_role_permission.format(id_role_template, id_permission)
        DBBroker.send_sql(sql_text, connection)
        connection.commit()
        connection.close()
        return {}

    def _get_role(self, id_role, *args, **kwargs):
        connection = DBBroker.get_connection(self.config_path)
        sql_text = cs.SQL.get_role.format(id_role)
        result = DBBroker.send_select(sql_text, connection)

        if len(result) == 0:
            raise RoleException('Role with id = {} not found'.format(id_role), code=cs.StatusCodes.not_found_code)
        if len(result) > 1:
            raise RoleException('Found more than one role with id = {}'.format(id_role))
        return APISchema.validate_list_to_dict(list(result[0]), APISchema.get_role_schema)

    def _role_exists(self, id_role, *args, **kwargs):
        try:
            self._get_role(id_role=id_role)
        except RoleException as r:
            if r.code == cs.StatusCodes.not_found_code:
                return False
            raise r
        return True

    def _get_role_type(self, id_role, *args, **kwargs):
        role = self._get_role(id_role=id_role, *args, **kwargs)
        return self._get_role_template(role['id_role_template'])['id_role_type']

    def _get_role_data(self, id_role, *args, **kwargs):
        if not self._role_exists(id_role=id_role):
            raise RoleException('Role with id = {} not found'.format(id_role), code=cs.StatusCodes.not_found_code)

        connection = DBBroker.get_connection(self.config_path)
        sql_text = cs.SQL.get_role_data.format(id_role)
        result = DBBroker.send_select(sql_text, connection)
        connection.close()
        return APISchema.validate_list_to_dict(list(result[0]), APISchema.get_role_data_schema)

    def _get_role_data_grouped(self, id_role, *args, **kwargs):
        role = self._get_role_data(id_role=id_role, *args, **kwargs)
        return Utils.group_role_list([role])[0]

    def _get_role_list(self, order_by=cs.DefaultSort.order_by.format('role'), order_dir=cs.DefaultSort.order_dir,
                       limit=cs.DefaultSort.limit, offset=cs.DefaultSort.offset, *args, **kwargs):
        connection = DBBroker.get_connection(self.config_path)
        sql_text = cs.SQL.get_role_list.format(order_by, order_dir, limit, offset)
        result = DBBroker.send_select(sql_text, connection)
        connection.close()
        return [APISchema.validate_list_to_dict(list(r), APISchema.get_role_data_schema) for r in result]

    def _get_role_list_grouped(self, order_by=cs.DefaultSort.order_by.format('role'),
                               order_dir=cs.DefaultSort.order_dir,
                               limit=cs.DefaultSort.limit, offset=cs.DefaultSort.offset, *args, **kwargs):
        role_list = self._get_role_list(order_by=order_by, order_dir=order_dir, limit=limit, offset=offset)
        return Utils.group_role_list(role_list)

    def _get_free_id_role(self):
        free_id_role = 1
        while free_id_role in [role['id_role'] for role in self._get_role_list_grouped()]:
            free_id_role += 1
        return free_id_role

    def _create_role(self, role_name, id_pool=None, permissions=None, *args, **kwargs):
        permissions = [] if permissions is None else permissions
        role_template = self._create_role_template(id_role_type=2 if id_pool is not None else 1)
        for id_permission in permissions:
            self._create_role_permission(id_role_template=role_template['id_role_template'],
                                         id_permission=id_permission)

        id_role = self._get_free_id_role()

        connection = DBBroker.get_connection(self.config_path)
        sql_text = cs.SQL.create_role.format(id_role, role_name, Utils.check_null(id_pool),
                                             role_template['id_role_template'])
        DBBroker.send_sql(sql_text, connection)
        connection.commit()
        connection.close()
        return self._get_role_data_grouped(id_role=id_role)

    def _delete_role(self, id_role, *args, **kwargs):
        role_data = self._get_role_data_grouped(id_role=id_role)
        role = self._get_role(id_role=id_role)
        for id_permission in [role_permission['id_permission'] for role_permission in role_data['permissions']]:
            self._delete_role_permission(id_role_template=role['id_role_template'], id_permission=id_permission)

        self._delete_role_template(id_role_template=role['id_role_template'])
        connection = DBBroker.get_connection(self.config_path)
        sql_text = cs.SQL.delete_role.format(id_role)
        DBBroker.send_sql(sql_text, connection)
        connection.commit()
        connection.close()
        return {}

    def _add_permission_to_role(self, id_role, id_permission, *args, **kwargs):
        role = self._get_role(id_role=id_role)
        self._create_role_permission(id_role_template=role['id_role_template'], id_permission=id_permission)
        return self._get_role_data_grouped(id_role=id_role)

    def _delete_permission_from_role(self, id_role, id_permission):
        role = self._get_role(id_role=id_role)
        self._delete_role_permission(id_role_template=role['id_role_template'], id_permission=id_permission)
        return self._get_role_data_grouped(id_role=id_role)

    def _set_role_name(self, id_role, role_name, *args, **kwargs):
        role = self._get_role(id_role=id_role)
        connection = DBBroker.get_connection(self.config_path)
        sql_text = cs.SQL.set_role_name.format(role_name, id_role)
        DBBroker.send_sql(sql_text, connection)
        connection.commit()
        connection.close()
        return self._get_role_data_grouped(id_role=id_role)

    def _set_role_type(self, id_role_template, id_role_type, *args, **kwargs):
        connection = DBBroker.get_connection(self.config_path)
        sql_text = cs.SQL.set_role_type.format(id_role_type, id_role_template)
        DBBroker.send_sql(sql_text, connection)
        connection.commit()
        connection.close()
        return self._get_role_template(id_role_template=id_role_template)

    def _set_role_pool(self, id_role, id_pool=None, *args, **kwargs):
        role = self._get_role(id_role=id_role)
        self._set_role_type(id_role_template=role['id_role_template'], id_role_type=1 if id_pool is None else 2)
        connection = DBBroker.get_connection(self.config_path)
        sql_text = cs.SQL.set_role_pool.format(Utils.check_null(id_pool), id_role)
        DBBroker.send_sql(sql_text, connection)
        connection.commit()
        connection.close()
        return self._get_role_data_grouped(id_role=id_role)

    def _modify_role(self, id_role, *args, **kwargs):
        if 'emp' in kwargs:
            for id_permission in kwargs['emp']:
                self._add_permission_to_role(id_role=id_role, id_permission=id_permission)
        if 'abol' in kwargs:
            for id_permission in kwargs['abol']:
                self._delete_permission_from_role(id_role=id_role, id_permission=id_permission)
        if 'role_name' in kwargs:
            self._set_role_name(id_role=id_role, role_name=kwargs['role_name'])
        if 'id_pool' in kwargs:
            self._set_role_pool(id_role=id_role, id_pool=kwargs['id_pool'])
        return self._get_role_data_grouped(id_role=id_role)

    def _get_user_role_list(self, id_vdi_user, *args, **kwargs):
        connection = DBBroker.get_connection(self.config_path)
        sql_text = cs.SQL.get_user_role_list.format(id_vdi_user)
        result = DBBroker.send_select(sql_text, connection)
        connection.close()
        return [APISchema.validate_list_to_dict(list(role), APISchema.get_user_role_list_schema) for role in result]

    def _get_user_role_list_grouped(self, id_vdi_user, *args, **kwargs):
        user_role_list = self._get_user_role_list(id_vdi_user=id_vdi_user)
        return Utils.group_user_role_list(user_role_list)

    @AuthControl.authorized
    def get_role_data(self, id_role, *args, **kwargs):
        return self._get_role_data_grouped(id_role=id_role, *args, **kwargs)

    @AuthControl.authorized
    def get_role_list(self, order_by=cs.DefaultSort.order_by.format('role'), order_dir=cs.DefaultSort.order_dir,
                      limit=cs.DefaultSort.limit, offset=cs.DefaultSort.offset, *args, **kwargs):
        return self._get_role_list_grouped(order_by=order_by, order_dir=order_dir, limit=limit, offset=offset)

    @AuthControl.authorized
    def create_role(self, role_name, id_pool=None, permissions=None, *args, **kwargs):
        return self._create_role(role_name=role_name, id_pool=id_pool, permissions=permissions, *args, **kwargs)

    @AuthControl.authorized
    def modify_role(self, id_role, *args, **kwargs):
        return self._modify_role(id_role=id_role, *args, **kwargs)

    @AuthControl.authorized
    def delete_role(self, id_role, *args, **kwargs):
        return self._delete_role(id_role=id_role, *args, **kwargs)

    def _count(self, *args, **kwargs):
        return len(self._get_role_list_grouped())

    @staticmethod
    def search_action_in_permissions(permissions, id_object, id_action=1):
        access = [
            int(p_permission['id_pool']) if p_permission['id_pool'] != 'platform' else p_permission['id_pool']
            for p_permission in permissions
            if (id_object in [
                o['id_object'] for o in [
                    p for p in p_permission['permissions']
                    if id_action in p['actions']
                ]
            ])
        ]
        return 'platform' if 'platform' in access else access

    def _has_access_to_server(self, access, id_server=None):
        server = ServerControl(config_path=self.config_path)._get_server(id_server=id_server)
        return (server['id_pool'] is None) or (server['id_pool'] in access)

    def _has_access_to_user(self, access, id_vdi_user=None):
        user_pools = [user_permission['id_pool'] for user_permission in
                      self._get_user_permission_list_grouped(id_vdi_user=id_vdi_user)]
        for id_pool in user_pools:
            if id_pool in access:
                return True
        return False

    def _has_access_to_session(self, access, id_session=None):
        session = SessionControl(config_path=self.config_path)._get_session(id_session=id_session)
        return self._has_access_to_server(access, id_server=session['id_server'])

    def _has_access_to_object(self, access, id_object, id_field):
        if access == 'platform':
            return True
        if id_object == cs.ROLE.objects.pool:
            return id_field in access
        elif id_object == cs.ROLE.objects.server:
            return self._has_access_to_server(access, id_server=id_field)
        elif id_object == cs.ROLE.objects.user:
            return self._has_access_to_user(access, id_vdi_user=id_field)
        elif id_object == cs.ROLE.objects.session:
            return self._has_access_to_session(access, id_session=id_field)

    @staticmethod
    def check_role(id_object, id_action=cs.ROLE.actions.R, except_case=lambda *args, **kwargs: False):
        def decorator(method):
            def wrapper(*args, **kwargs):
                current_user = kwargs['current_user']
                control = RoleControl(config_path=args[0].config_path)
                permissions = PermissionControl()._get_user_permission_list_grouped(id_vdi_user=current_user)
                access = RoleControl.search_action_in_permissions(permissions, id_object, id_action=id_action)
                kwargs['access'] = access
                if (len(access) == 0) and not (except_case(*args, **kwargs)):
                    print('No access to object {}'.format(cs.ROLE.objects.object_name[id_object - 1]))
                    raise AccessException(id_object=id_object, id_action=id_action)
                field = 'id_{}'.format(cs.ROLE.objects.object_name[id_object - 1])
                if field in kwargs:
                    has_access = control._has_access_to_object(access, id_object, kwargs[field])
                    if (not has_access) and not (except_case(*args, **kwargs)):
                        print('No access to {} {}'.format(cs.ROLE.objects.object_name[id_object - 1], kwargs[field]))
                        raise AccessException(id_object=id_object, id_action=id_action, current_object=kwargs[field])
                journal = dict()
                result = method(*args, **kwargs)
                if len(result) and isinstance(result, tuple) == 2:
                    journal = result[1]
                    result = result[0]
                journal['id_object'] = id_object
                journal['id_action'] = id_action
                if field in kwargs:
                    journal['current_object'] = kwargs[field]
                return result, journal

            return wrapper

        return decorator


class JournalControl(RoleControl):
    def _get_journal_record(self, id_journal_record=None, *args, **kwargs):
        connection = DBBroker.get_connection(self.config_path)
        sql_text = cs.SQL.get_gournal_record.format(id_journal_record)
        result = DBBroker.send_select(sql_text, connection)
        connection.close()
        if len(result) == 0:
            raise JournalException('Journal record with id = {} not found'.format(id_journal_record),
                                   code=cs.StatusCodes.not_found_code)
        if len(result) > 1:
            raise JournalException('Found more than one journal record with id = {}'.format(id_journal_record))
        return APISchema.validate_list_to_dict(list(result[0]), APISchema.get_journal_record_schema)

    def _journal_record_exists(self, id_journal_record=None, *args, **kwargs):
        try:
            self._get_journal_record(id_journal_record=id_journal_record)
        except JournalException as u:
            if u.code == cs.StatusCodes.not_found_code:
                return False
            raise u

    def _get_journal_record_data(self, id_journal_record=None, *args, **kwargs):
        self._get_journal_record(id_journal_record=id_journal_record)
        connection = DBBroker.get_connection(self.config_path)
        sql_text = cs.SQL.get_gournal_record_data.format(id_journal_record)
        result = DBBroker.send_select(sql_text, connection)
        connection.close()
        return APISchema.validate_list_to_dict(list(result[0]), APISchema.get_journal_record_data_schema)

    def _get_journal_record_list(self, access='platform', current_user=None,
                                 order_by=cs.DefaultSort.order_by.format('journal_record'),
                                 order_dir=cs.DefaultSort.order_dir,
                                 limit=cs.DefaultSort.limit, offset=cs.DefaultSort.offset, *args, **kwargs):
        connection = DBBroker.get_connection(self.config_path)
        sql_text = cs.SQL.get_journal_record_list.format(order_by, order_dir, limit, offset)
        if access != 'platform':
            sql_text = cs.SQL.get_journal_record_list_pool_only.format(
                ' OR '.join(['id_pool={}'.format(a) for a in access]) + (' OR ' if len(
                    ['role.id_pool={}'.format(a) for a in
                     access]) != 0 else '') + 'journal_record.id_vdi_user={}'.format(
                    current_user), order_by, order_dir, limit, offset)
        result = DBBroker.send_select(sql_text, connection)
        connection.close()
        return [APISchema.validate_list_to_dict(list(r), APISchema.get_journal_record_data_schema) for r in result]

    def _create_journal_record(self, id_vdi_user=None, id_client=None, id_object=None, id_action=None,
                               current_object=None, success=True,
                               description=None,
                               *args, **kwargs):
        connection = DBBroker.get_connection(self.config_path)
        sql_text = cs.SQL.create_journal_record.format(Utils.check_null(id_vdi_user, do_is=False),
                                                       id_client,
                                                       Utils.check_null(id_object, do_is=False),
                                                       Utils.check_null(id_action, do_is=False),
                                                       Utils.check_null(current_object
                                                                        if current_object is None
                                                                        else
                                                                        str(current_object), do_is=False),
                                                       'TRUE' if success else 'FALSE',
                                                       description)
        DBBroker.send_sql(sql_text, connection)
        connection.commit()
        return {}

    @AuthControl.authorized
    def get_journal_record_list(self, access='platform', current_user=None,
                                order_by=cs.DefaultSort.order_by.format('journal_record'),
                                order_dir=cs.DefaultSort.order_dir,
                                limit=cs.DefaultSort.limit, offset=cs.DefaultSort.offset, *args, **kwargs):
        return self._get_journal_record_list(access=access, current_user=current_user, order_by=order_by,
                                             order_dir=order_dir, limit=limit, offset=offset, *args, **kwargs)


class UserControl(RoleControl):

    def _sign_in(self, user_name: str, hashed_pass: str, *args, **kwargs):
        if not self._user_exists(user_name=user_name):
            raise AuthException('User with user name \'{}\' does not exist'.format(user_name))
        user = self._get_user(user_name=user_name)
        if user['password'] != hashed_pass:
            raise AuthException('Wrong password for user "{}"'.format(user_name))

        id_token = Utils.generate_token()

        connection = DBBroker.get_connection(self.config_path)
        sql_text = cs.SQL.sign_in.format(id_token, user['id_vdi_user'])
        DBBroker.send_sql(sql_text, connection)
        connection.commit()
        connection.close()
        print(user)
        return {'auth_token': id_token}, {'current_user': user['id_vdi_user'], 'id_object': cs.ROLE.objects.user,
                                          'id_action': cs.ROLE.actions.auth, 'current_object': user['id_vdi_user']}

    def sign_in(self, user_name: str, hashed_pass: str, *args, **kwargs):
        res = self._sign_in(user_name=user_name, hashed_pass=hashed_pass)
        return res

    def _sign_out(self, *args, **kwargs):
        id_vdi_user = AuthControl.search_token(self.id_token, self.config_path)['id_vdi_user']
        connection = DBBroker.get_connection(self.config_path)
        sql_text = cs.SQL.sign_out.format(self.id_token)
        DBBroker.send_sql(sql_text, connection)
        connection.commit()
        connection.close()
        return {'sign_out': True}, {'current_user': id_vdi_user, 'id_object': cs.ROLE.objects.user,
                                    'id_action': cs.ROLE.actions.deauth}

    @AuthControl.authorized
    def sign_out(self, *args, **kwargs):
        return self._sign_out(*args, **kwargs)

    def _get_user(self, id_vdi_user=None, user_name=None, *args, **kwargs):
        if (id_vdi_user is None) and (user_name is None):
            raise UserException('Expected user id_vdi_user or user name', cs.StatusCodes.bad_request_code)
        n = 'id_vdi_user' if id_vdi_user is not None else 'user_name'
        p = id_vdi_user if id_vdi_user is not None else '\'{}\''.format(user_name)

        connection = DBBroker.get_connection(self.config_path)
        sql_text = cs.SQL.get_user.format(n, p)
        users = DBBroker.send_select(sql_text, connection)
        connection.close()
        if len(users) == 0:
            raise UserException('User with {} = {} not found'.format(n, p), code=cs.StatusCodes.not_found_code)
        if len(users) > 1:
            raise UserException('Found more than one user with {} = {}'.format(n, p))
        return APISchema.validate_list_to_dict(list(users[0]), APISchema.get_user_schema)

    def _user_exists(self, id_vdi_user=None, user_name=None, *args, **kwargs):
        try:
            self._get_user(id_vdi_user=id_vdi_user, user_name=user_name)
        except UserException as u:
            if u.code == cs.StatusCodes.not_found_code:
                return False
            raise u
        except Exception as e:
            raise MethodException(str(e))
        return True

    def _get_user_list(self, order_by=cs.DefaultSort.order_by.format('vdi_user'), order_dir=cs.DefaultSort.order_dir,
                       limit=cs.DefaultSort.limit, offset=cs.DefaultSort.offset, *args, **kwargs):
        connection = DBBroker.get_connection(self.config_path)
        access = kwargs['access'] if 'access' in kwargs else 'platform'
        if access == 'platform':
            sql_text = cs.SQL.get_user_list.format(order_by, order_dir, limit, offset)
        else:
            sql_text = cs.SQL.get_user_list_pool_only.format(
                ' OR '.join(['role_instance.id_pool = {}'.format(p) for p in access]), order_by, order_dir, limit,
                offset)
        result = DBBroker.send_select(sql_text, connection)
        users = [APISchema.validate_list_to_dict(list(u), APISchema.get_user_list_schema) for u in result]
        for user in users:
            user['pool_roles'] = self._get_user_role_list_grouped(id_vdi_user=user['id_vdi_user'])
        return users

    @AuthControl.authorized
    @RoleControl.check_role(cs.ROLE.objects.user)
    def get_user_list(self, order_by=cs.DefaultSort.order_by.format('vdi_user'),
                      order_dir=cs.DefaultSort.order_dir,
                      limit=cs.DefaultSort.limit, offset=cs.DefaultSort.offset, *args, **kwargs):
        return self._get_user_list(order_by=order_by, order_dir=order_dir, limit=limit, offset=offset, *args, **kwargs)

    def _create_user(self, user_name=None, hashed_password=None, name=None, last_name=None,
                     email=None, *args, **kwargs):
        if self._user_exists(user_name=user_name):
            raise UserCreateException('User with user name \'{}\' already exists'.format(user_name))

        if len(user_name) < 3:
            raise UserCreateException('User name "{}" is shorter than 3 characters'.format(user_name),
                                      code=cs.StatusCodes.bad_request_code)

        pattern = re.compile('\w*[A-Za-z]\w*')
        mis_pattern = re.compile('\W')

        invalid = mis_pattern.findall(user_name)

        if len(invalid) != 0:
            raise UserCreateException(
                'Invalid character "{}" at index {} in user name "{}"'.format(invalid[0], user_name.index(invalid[0]),
                                                                              user_name),
                cs.StatusCodes.bad_request_code)

        if pattern.fullmatch(user_name) is None:
            raise UserCreateException('User name must contain at least 1 latin letter',
                                      code=cs.StatusCodes.bad_request_code)

        name = Utils.check_null(name)
        last_name = Utils.check_null(last_name)
        email = Utils.check_null(email)

        connection = DBBroker.get_connection(self.config_path)
        sql_text = cs.SQL.create_user.format(name, last_name, email, user_name, hashed_password)
        DBBroker.send_sql(sql_text, connection)
        connection.commit()
        connection.close()

        id_vdi_user = self._get_user(user_name=user_name)['id_vdi_user']
        return self._get_user_data(id_vdi_user=id_vdi_user)

    @AuthControl.authorized
    @RoleControl.check_role(cs.ROLE.objects.user, id_action=cs.ROLE.actions.C)
    def create_user(self, user_name: str, password: str, name=None, last_name=None,
                    email=None, *args, **kwargs):
        self._create_user(user_name=user_name, hashed_password=password, name=name, last_name=last_name,
                          email=email, *args, **kwargs)
        user = self._get_user(user_name=user_name)
        if 'emp' in kwargs:
            for role in kwargs['emp']:
                self._add_user_role(id_vdi_user=user['id_vdi_user'], id_role=role)
        return self._get_user_data(user['id_vdi_user'])

    def _get_user_data(self, id_vdi_user=None, *args, **kwargs):
        connection = DBBroker.get_connection(self.config_path)
        sql_text = cs.SQL.get_user_data.format(id_vdi_user)
        result = DBBroker.send_select(sql_text, connection)
        connection.close()
        user = APISchema.validate_list_to_dict(list(result[0]), APISchema.get_user_list_schema)
        user['pool_roles'] = self._get_user_role_list_grouped(id_vdi_user=user['id_vdi_user'])
        return user

    @AuthControl.authorized
    @RoleControl.check_role(cs.ROLE.objects.user,
                            except_case=Utils.user_is_current)
    def get_user_data(self, id_vdi_user=None, *args, **kwargs):
        return self._get_user_data(id_vdi_user=id_vdi_user, *args, **kwargs)

    @AuthControl.authorized
    def get_my_data(self, *args, **kwargs):
        return self._get_user_data(id_vdi_user=kwargs['current_user'], *args, **kwargs), {
            'id_object': cs.ROLE.objects.user, 'id_action': cs.ROLE.actions.R
        }

    def _user_has_role(self, id_vdi_user, id_role, *args, **kwargs):
        user = self._get_user_data(id_vdi_user=id_vdi_user, *args, **kwargs)
        for p in [p_role for p_role in user['pool_roles']]:
            if id_role in [role['id_role'] for role in p['roles']]:
                return True
        return False

    def _add_user_role(self, id_vdi_user, id_role, *args, **kwargs):
        if self._user_has_role(id_vdi_user=id_vdi_user, id_role=id_role):
            return self._get_user_data(id_vdi_user=id_vdi_user)

        role = self._get_role(id_role)
        connection = DBBroker.get_connection(self.config_path)

        sql_text = cs.SQL.add_user_role.format(id_vdi_user, id_role)
        DBBroker.send_sql(sql_text, connection)
        connection.commit()
        connection.close()
        return self._get_user_data(id_vdi_user=id_vdi_user)

    def _delete_user_role(self, id_vdi_user, id_role, *args, **kwargs):
        if not self._user_exists(id_vdi_user=id_vdi_user):
            raise UserException('User with id {} does not exist'.format(id_vdi_user),
                                code=cs.StatusCodes.bad_request_code)

        connection = DBBroker.get_connection(self.config_path)
        sql_text = cs.SQL.delete_user_role.format(id_vdi_user, id_role)
        DBBroker.send_sql(sql_text, connection)
        connection.commit()
        connection.close()
        return self._get_user_data(id_vdi_user=id_vdi_user)

    @AuthControl.authorized
    @RoleControl.check_role(cs.ROLE.objects.user, id_action=cs.ROLE.actions.U)
    def add_user_role(self, id_vdi_user, id_role, *args, **kwargs):
        return self._add_user_role(id_vdi_user, id_role, *args, **kwargs)

    @AuthControl.authorized
    @RoleControl.check_role(cs.ROLE.objects.user, id_action=cs.ROLE.actions.U)
    def delete_user_role(self, id_vdi_user, id_role, *args, **kwargs):
        return self._delete_user_role(id_vdi_user, id_role, *args, **kwargs)

    def _set_name(self, id_vdi_user, name):
        connection = DBBroker.get_connection(self.config_path)
        sql_text = cs.SQL.set_name.format(name, id_vdi_user)
        DBBroker.send_sql(sql_text, connection)
        connection.commit()
        connection.close()

    def _set_last_name(self, id_vdi_user, last_name):
        connection = DBBroker.get_connection(self.config_path)
        sql_text = cs.SQL.set_last_name.format(last_name, id_vdi_user)
        DBBroker.send_sql(sql_text, connection)
        connection.commit()
        connection.close()

    def _set_email(self, id_vdi_user, email):
        connection = DBBroker.get_connection(self.config_path)
        sql_text = cs.SQL.set_email.format(email, id_vdi_user)
        DBBroker.send_sql(sql_text, connection)
        connection.commit()
        connection.close()

    def _set_user_name(self, id_vdi_user, user_name):
        connection = DBBroker.get_connection(self.config_path)
        sql_text = cs.SQL.set_user_name.format(user_name, id_vdi_user)
        DBBroker.send_sql(sql_text, connection)
        connection.commit()
        connection.close()

    def _set_password(self, id_vdi_user, password):
        connection = DBBroker.get_connection(self.config_path)
        sql_text = cs.SQL.set_password.format(password, id_vdi_user)
        DBBroker.send_sql(sql_text, connection)
        connection.commit()
        connection.close()

    def _modify_user(self, id_vdi_user=None, *args, **kwargs):
        if ('name' in kwargs) and (kwargs['name'] is not None):
            self._set_name(id_vdi_user, kwargs['name'])
        if ('last_name' in kwargs) and (kwargs['last_name'] is not None):
            self._set_last_name(id_vdi_user, kwargs['last_name'])
        if ('email' in kwargs) and (kwargs['email'] is not None):
            self._set_email(id_vdi_user, kwargs['email'])
        if ('password' in kwargs) and (kwargs['password'] is not None):
            self._set_password(id_vdi_user, kwargs['password'])
        if ('user_name' in kwargs) and (kwargs['user_name'] is not None) and self._user_exists(
                user_name=kwargs['user_name']):
            self._set_user_name(id_vdi_user, kwargs['user_name'])

    @AuthControl.authorized
    @RoleControl.check_role(cs.ROLE.objects.user, id_action=cs.ROLE.actions.U)
    def modify_user(self, id_vdi_user=None, *args, **kwargs):
        self._modify_user(id_vdi_user=id_vdi_user, *args, **kwargs)

    def _get_user_sessions(self, id_vdi_user, *args, **kwargs):
        if not self._user_exists(id_vdi_user=id_vdi_user):
            raise UserException('User with id = {} does not exist'.format(id_vdi_user))

        connection = DBBroker.get_connection(self.config_path)
        sql_text = cs.SQL.get_user_sessions.format(id_vdi_user)
        result = DBBroker.send_select(sql_text, connection)
        connection.close()
        return [r[0] for r in result]

    def _delete_user(self, id_vdi_user, *args, **kwargs):
        sessions = self._get_user_sessions(id_vdi_user)
        SessionControl()._close_list_of_sessions(sessions)

        connection = DBBroker.get_connection(self.config_path)
        sql_text = cs.SQL.delete_user.format(id_vdi_user)
        DBBroker.send_sql(sql_text, connection)
        connection.commit()
        connection.close()
        return {}

    @AuthControl.authorized
    @RoleControl.check_role(cs.ROLE.objects.user, id_action=cs.ROLE.actions.D)
    def delete_user(self, id_vdi_user, *args, **kwargs):
        return self._delete_user(id_vdi_user=id_vdi_user, *args, **kwargs)

    def _get_user_pool_roles(self, id_vdi_user, id_pool):
        if not PoolControl()._pool_exists(id_pool=id_pool):
            raise PoolException('Pool with id = {} does not exist'.format(id_pool),
                                code=cs.StatusCodes.not_found_code)
        user_roles = self._get_user_role_list_grouped(id_vdi_user=id_vdi_user)
        pool_user_roles = [pool_role for pool_role in user_roles if pool_role['id_pool'] == id_pool]
        if len(pool_user_roles) == 0:
            return []
        return [role['id_role'] for role in pool_user_roles[0]]

    def _count(self):
        return len(self._get_user_list())


class ServerControl(UserControl):

    def _get_server(self, id_server=None, ip=None, *args, **kwargs):
        n = 'id_server' if id_server is not None else 'ip'
        p = id_server if id_server is not None else '\'{}\''.format(ip)
        connection = DBBroker.get_connection(self.config_path)
        sql_text = cs.SQL.get_server.format(n, p)
        result = DBBroker.send_select(sql_text, connection)
        connection.close()
        if len(result) == 0:
            raise ServerException('Server with {} = {} does not exist'.format(n, p), code=cs.StatusCodes.not_found_code)
        if len(result) != 1:
            raise ServerException('Found more then one server with {} = {}'.format(n, p),
                                  code=cs.StatusCodes.internal_server_error_code)

        return APISchema.validate_list_to_dict(list(result[0]), APISchema.get_server_schema)

    def _server_exists(self, id_server=None, ip=None, *args, **kwargs):
        try:
            self._get_server(id_server=id_server, ip=ip, *args, **kwargs)
        except ServerException as s:
            if s.code == cs.StatusCodes.not_found_code:
                return False
            raise s
        except Exception as e:
            raise e
        return True

    def _create_server(self, server_name: str, ip: str, login: str, password: str):
        if self._server_exists(ip=ip):
            raise ServerException('Server with ip = {} already exists'.format(ip), code=cs.StatusCodes.bad_request_code)

        init_result = VDI.Server.init(server_name, ip, login, password)
        if not init_result['success']:
            raise ServerCreateException(init_result['message'], code=cs.StatusCodes.internal_server_error_code)

        connection = DBBroker.get_connection(self.config_path)
        sql_text = cs.SQL.create_server.format(server_name, ip, login, password)
        DBBroker.send_sql(sql_text, connection)
        connection.commit()
        connection.close()

        return self._get_server_data(self._get_server(ip=ip)['id_server'])

    def _get_server_data(self, id_server, *args, **kwargs):
        if not self._server_exists(id_server=id_server):
            raise ServerException('Server with id = {} does not exist'.format(id_server),
                                  code=cs.StatusCodes.not_found_code)

        connection = DBBroker.get_connection(self.config_path)
        sql_text = cs.SQL.get_server_data.format(id_server)
        result = DBBroker.send_select(sql_text, connection)
        connection.close()
        r = APISchema.validate_list_to_dict(list(result[0]), APISchema.get_server_data_schema)
        r['load'] = 0
        return r

    def _get_server_list(self, order_by=cs.DefaultSort.order_by.format('server'),
                         order_dir=cs.DefaultSort.order_dir,
                         limit=cs.DefaultSort.limit, offset=cs.DefaultSort.offset, *args, **kwargs):
        connection = DBBroker.get_connection(self.config_path)
        access = kwargs['access'] if 'access' in kwargs else 'platform'
        if access == 'platform':
            sql_text = cs.SQL.get_server_list.format(order_by, order_dir, limit, offset)
        else:
            sql_text = cs.SQL.get_server_list_pool_only.format(' OR '.join(
                ['server.id_pool = {}'.format(id_pool) for id_pool in access] + ['server.id_pool is NULL']),
                order_by, order_dir, limit, offset)
        result = DBBroker.send_select(sql_text, connection)
        connection.close()
        res = []
        for r in [APISchema.validate_list_to_dict(list(r), APISchema.get_server_data_schema) for r in result]:
            r['load'] = 0
            res.append(r)
        return res

    @AuthControl.authorized
    @RoleControl.check_role(cs.ROLE.objects.server)
    def get_server_data(self, id_server, pool_only=False, *args, **kwargs):
        return self._get_server_data(id_server=id_server, *args, **kwargs)

    @AuthControl.authorized
    @RoleControl.check_role(cs.ROLE.objects.server)
    def get_server_list(self, order_by=cs.DefaultSort.order_by.format('server'),
                        order_dir=cs.DefaultSort.order_dir,
                        limit=cs.DefaultSort.limit, offset=cs.DefaultSort.offset, *args, **kwargs):
        return self._get_server_list(order_by=order_by, order_dir=order_dir, limit=limit, offset=offset, *args,
                                     **kwargs)

    @AuthControl.authorized
    @RoleControl.check_role(cs.ROLE.objects.server, id_action=cs.ROLE.actions.C)
    def create_server(self, server_name: str, ip: str, login: str, password: str, *args, **kwargs):
        return self._create_server(server_name, ip, login, password)

    def _get_server_sessions(self, id_server, *args, **kwargs):
        if not self._server_exists(id_server=id_server):
            raise ServerException('Server with id = {} does not exist'.format(id_server))

        connection = DBBroker.get_connection(self.config_path)
        sql_text = cs.SQL.get_server_sessions.format(id_server)
        result = DBBroker.send_select(sql_text, connection)
        connection.close()
        return [r[0] for r in
                result]  # [APISchema.validate_list_to_dict(list(r), APISchema.get_server_sessions_schema) for r in result]

    def _server_has_ad_settings(self, id_server):
        server = self._get_server(id_server)
        return (server['ad_login'] is not None) and (server['ad_password'] is not None)

    def _server_is_connected_to_pool(self, id_server):
        server = self._get_server(id_server)
        return server['id_pool'] is not None

    def _set_server_ad_settings(self, id_server, ad_login, ad_password, *args, **kwargs):
        connection = DBBroker.get_connection(self.config_path)
        sql_text = cs.SQL.set_server_ad_settings.format(Utils.check_null(ad_login), Utils.check_null(ad_password),
                                                        id_server)
        DBBroker.send_sql(sql_text, connection)
        connection.commit()
        connection.close()
        return self._get_server_data(id_server)

    def _connect_server_to_pool(self, id_server, id_pool, *args, **kwargs):
        if not self._server_has_ad_settings(id_server):
            raise ServerException('Server with id {} has no AD settings'.format(id_server),
                                  code=cs.StatusCodes.bad_request_code)
        if self._server_is_connected_to_pool(id_server):
            raise ServerException('Server with id {} is already connected to some pool'.format(id_server))
        connection = DBBroker.get_connection(self.config_path)
        sql_text = cs.SQL.set_server_pool.format(id_pool, id_server)
        DBBroker.send_sql(sql_text, connection)
        connection.commit()
        connection.close()
        return self._get_server_data(id_server)

    def _disconnect_server_from_pool(self, id_server, *args, **kwargs):
        sessions = self._get_server_sessions(id_server)
        SessionControl()._close_list_of_sessions(sessions)
        self._set_server_ad_settings(id_server=id_server, ad_login=None, ad_password=None)
        connection = DBBroker.get_connection(self.config_path)
        sql_text = cs.SQL.set_server_pool.format('NULL', id_server)
        DBBroker.send_sql(sql_text, connection)
        connection.commit()
        connection.close()
        return self._get_server_data(id_server)

    def _disconnect_list_of_servers_from_pool(self, servers, *args, **kwargs):
        for id_server in servers:
            self._disconnect_server_from_pool(id_server=id_server)

    def _delete_server(self, id_server, *args, **kwargs):
        self._disconnect_server_from_pool(id_server)
        connection = DBBroker.get_connection(self.config_path)
        sql_text = cs.SQL.delete_server.format(id_server)
        DBBroker.send_sql(sql_text, connection)
        connection.commit()
        connection.close()
        return {}

    @AuthControl.authorized
    @RoleControl.check_role(cs.ROLE.objects.server, id_action=cs.ROLE.actions.U)
    def set_server_ad_settings(self, id_server, ad_login, ad_password, *args, **kwargs):
        return self._set_server_ad_settings(id_server, ad_login, ad_password, *args, **kwargs)

    @AuthControl.authorized
    @RoleControl.check_role(cs.ROLE.objects.server, id_action=cs.ROLE.actions.U)
    def connect_server_to_pool(self, id_server=None, id_pool=None, *args, **kwargs):
        return self._connect_server_to_pool(id_server=id_server, id_pool=id_pool, *args, **kwargs)

    @AuthControl.authorized
    @RoleControl.check_role(cs.ROLE.objects.server, id_action=cs.ROLE.actions.U)
    def disconnect_server_from_pool(self, id_server=None, *args, **kwargs):
        return self._disconnect_server_from_pool(id_server=id_server, *args, **kwargs)

    @AuthControl.authorized
    @RoleControl.check_role(cs.ROLE.objects.server, id_action=cs.ROLE.actions.D)
    def delete_server(self, id_server=None, *args, **kwargs):
        return self._delete_server(id_server=id_server, *args, **kwargs)

    def _count(self):
        return len(self._get_server_list())


class SessionControl(ServerControl):
    def _get_session(self, id_session, *args, **kwargs):
        connection = DBBroker.get_connection(self.config_path)
        sql_text = cs.SQL.get_session.format(id_session)
        result = DBBroker.send_select(sql_text, connection)
        if len(result) == 0:
            raise SessionException('Session with id = {} does not exist'.format(id_session),
                                   code=cs.StatusCodes.not_found_code)
        if len(result) != 1:
            raise SessionException('Found more then one session with id = {}'.format(id_session),
                                   code=cs.StatusCodes.internal_server_error_code)

        return APISchema.validate_list_to_dict(list(result[0]), APISchema.get_session_schema)

    def _session_exists(self, id_session, *args, **kwargs):
        try:
            self._get_session(id_session=id_session)
            return True
        except SessionException as s:
            if s.code == cs.StatusCodes.not_found_code:
                return False
            raise s
        except Exception as e:
            raise e

    def _create_session(self, id_session, id_vdi_user, id_client, id_server, *args, **kwargs):
        if self._session_exists(id_session=id_session):
            raise SessionException('Session with id = {} already exists'.format(id_session),
                                   code=cs.StatusCodes.bad_request_code)
        if not self._user_exists(id_vdi_user=id_vdi_user):
            raise SessionException('User with id = {} does not exist'.format(id_vdi_user),
                                   code=cs.StatusCodes.not_found_code)
        if not self._server_exists(id_server=id_server):
            raise SessionException('Server with id = {} does not exist'.format(id_server),
                                   code=cs.StatusCodes.not_found_code)

        connection = DBBroker.get_connection(self.config_path)
        sql_text = cs.SQL.create_session.format(id_session, id_vdi_user, id_client, id_server)
        DBBroker.send_sql(sql_text, connection)
        connection.commit()
        connection.close()

        return self._get_session_data(id_session)

    def _get_session_data(self, id_session, *args, **kwargs):
        if not self._session_exists(id_session=id_session):
            raise SessionException('Session with id = {} does not exist'.format(id_session),
                                   code=cs.StatusCodes.not_found_code)

        connection = DBBroker.get_connection(self.config_path)
        sql_text = cs.SQL.get_session_data.format(id_session)
        result = DBBroker.send_select(sql_text, connection)
        connection.close()
        r = APISchema.validate_list_to_dict(list(result[0]), APISchema.get_session_data_schema)
        return r

    def _get_session_list(self, order_by=cs.DefaultSort.order_by.format('session'), order_dir=cs.DefaultSort.order_dir,
                          limit=cs.DefaultSort.limit, offset=cs.DefaultSort.offset, *args, **kwargs):
        connection = DBBroker.get_connection(self.config_path)
        access = kwargs['access'] if 'access' in kwargs else 'platform'
        if access == 'platform':
            sql_text = cs.SQL.get_session_list.format(order_by, order_dir, limit, offset)
        else:
            user_servers = [s['id_server'] for s in self._get_server_list(access=access)]
            sql_text = cs.SQL.get_session_list_pool_only.format(' OR '.join(
                ['session.id_server = {}'.format(id_server) for id_server in user_servers]),
                order_by, order_dir, limit, offset)
        result = DBBroker.send_select(sql_text, connection)
        connection.close()
        return [APISchema.validate_list_to_dict(list(r), APISchema.get_session_data_schema) for r in result]

    @AuthControl.authorized
    @RoleControl.check_role(cs.ROLE.objects.session)
    def get_session_list(self, order_by=cs.DefaultSort.order_by.format('session'), order_dir=cs.DefaultSort.order_dir,
                         limit=cs.DefaultSort.limit, offset=cs.DefaultSort.offset, *args, **kwargs):
        return self._get_session_list(order_by=order_by, order_dir=order_dir, limit=limit, offset=offset, *args,
                                      **kwargs)

    @AuthControl.authorized
    @RoleControl.check_role(cs.ROLE.objects.session)
    def get_session_data(self, id_session, *args, **kwargs):
        return self.get_session_data(id_session=id_session, *args, **kwargs)

    def _count(self):
        return len(self._get_session_list())

    def _close_session(self, id_session: str, *args, **kwargs):
        if not self._session_exists(id_session=id_session):
            raise SessionException('Session with id = {} does not exist'.format(id_session),
                                   code=cs.StatusCodes.not_found_code)

        connection = DBBroker.get_connection(self.config_path)
        sql_text = cs.SQL.close_session.format(id_session)
        DBBroker.send_sql(
            sql_text,
            connection)
        connection.commit()
        connection.close()
        return {'closed': id_session}

    def _block_session(self, id_session, *args, **kwargs):
        if not self._session_exists(id_session=id_session):
            raise SessionException('Session with id = {} does not exist'.format(id_session),
                                   code=cs.StatusCodes.not_found_code)

        connection = DBBroker.get_connection(self.config_path)
        sql_text = cs.SQL.set_session_state.format(3, id_session)
        DBBroker.send_sql(
            sql_text,
            connection)
        connection.commit()
        connection.close()
        return self._get_session_data(id_session=id_session, *args, **kwargs)

    def _unblock_session(self, id_session, *args, **kwargs):
        if not self._session_exists(id_session=id_session):
            raise SessionException('Session with id = {} does not exist'.format(id_session),
                                   code=cs.StatusCodes.not_found_code)

        connection = DBBroker.get_connection(self.config_path)
        sql_text = cs.SQL.set_session_state.format(1, id_session)
        DBBroker.send_sql(
            sql_text,
            connection)
        connection.commit()
        connection.close()
        return self._get_session_data(id_session=id_session, *args, **kwargs)

    @AuthControl.authorized
    @RoleControl.check_role(cs.ROLE.objects.session, id_action=cs.ROLE.actions.D)
    def close_session(self, id_session, *args, **kwargs):
        return self._close_session(id_session=id_session, *args, **kwargs)

    @AuthControl.authorized
    @RoleControl.check_role(cs.ROLE.objects.session, id_action=cs.ROLE.actions.U)
    def block_session(self, id_session, *args, **kwargs):
        return self._block_session(id_session=id_session, *args, **kwargs)

    @AuthControl.authorized
    @RoleControl.check_role(cs.ROLE.objects.session, id_action=cs.ROLE.actions.U)
    def unblock_session(self, id_session: str, *args, **kwargs):
        return self._unblock_session(id_session, *args, **kwargs)

    def _close_list_of_sessions(self, ids):
        for id_session in ids:
            self._close_session(id_session)


class PoolControl(SessionControl):

    def _get_pool(self, id_pool=None, pool_name=None, *args, **kwargs):
        n = 'id_pool' if id_pool is not None else 'pool_name'
        p = id_pool if id_pool is not None else '\'{}\''.format(pool_name)
        if (id_pool is None) and (pool_name is None) is None:
            raise PoolException('Expected id or AD name parameter for pool', code=cs.StatusCodes.bad_request_code)
        connection = DBBroker.get_connection(self.config_path)
        sql_text = cs.SQL.get_pool.format(n, p)
        result = DBBroker.send_select(sql_text, connection)
        if len(result) == 0:
            raise PoolException('Pool with {} = {} does not exist'.format(n, p),
                                code=cs.StatusCodes.not_found_code)
        if len(result) != 1:
            raise PoolException('Found more then one pool with id = {}'.format(id_pool),
                                code=cs.StatusCodes.internal_server_error_code)

        return APISchema.validate_list_to_dict(list(result[0]), APISchema.get_pool_schema)

    def _pool_exists(self, id_pool=None, pool_name=None, *args, **kwargs):
        try:
            self._get_pool(id_pool=id_pool, pool_name=pool_name)
            return True
        except PoolException as s:
            if s.code == cs.StatusCodes.not_found_code:
                return False
            raise s
        except Exception as e:
            raise e

    def _create_pool(self, pool_name, description, domain, *args, **kwargs):
        if self._pool_exists(pool_name=pool_name):
            raise SessionException('Pool with AD name = {} already exists'.format(pool_name),
                                   code=cs.StatusCodes.bad_request_code)
        connection = DBBroker.get_connection(self.config_path)
        sql_text = cs.SQL.create_pool.format(pool_name, description, domain)
        DBBroker.send_sql(sql_text, connection)
        connection.commit()
        connection.close()
        return self._get_pool_data(self._get_pool(pool_name=pool_name)['id_pool'])

    def _get_pool_servers(self, id_pool):
        if not self._pool_exists(id_pool=id_pool):
            raise PoolException('Pool with id = {} does not exist'.format(id_pool),
                                code=cs.StatusCodes.not_found_code)
        connection = DBBroker.get_connection(self.config_path)
        sql_text = cs.SQL.get_pool_servers.format(id_pool)
        result = DBBroker.send_select(sql_text, connection)
        connection.close()
        return [r[0] for r in result]

    def _get_pool_users(self, id_pool):
        if not self._pool_exists(id_pool=id_pool):
            raise PoolException('Pool with id = {} does not exist'.format(id_pool),
                                code=cs.StatusCodes.not_found_code)
        connection = DBBroker.get_connection(self.config_path)
        sql_text = cs.SQL.get_pool_users.format(id_pool)
        result = DBBroker.send_select(sql_text, connection)
        connection.close()
        return [r[0] for r in result]

    def _get_pool_roles(self, id_pool):
        if not self._pool_exists(id_pool=id_pool):
            raise PoolException('Pool with id = {} does not exist'.format(id_pool),
                                code=cs.StatusCodes.not_found_code)
        connection = DBBroker.get_connection(self.config_path)
        sql_text = cs.SQL.get_pool_roles.format(id_pool)
        result = DBBroker.send_select(sql_text, connection)
        connection.close()
        return [r[0] for r in result]

    def _connect_pool_to_broker(self, id_pool, *args, **kwargs):
        if not self._pool_exists(id_pool=id_pool):
            raise PoolException('Pool with id = {} does not exist'.format(id_pool),
                                code=cs.StatusCodes.not_found_code)
        connection = DBBroker.get_connection(self.config_path)
        sql_text = cs.SQL.set_pool_connected_2_broker.format('TRUE', id_pool)
        DBBroker.send_sql(sql_text, connection)
        connection.commit()
        connection.close()
        return self._get_pool_data(id_pool)

    def _disconnect_pool_from_broker(self, id_pool, *args, **kwargs):
        if not self._pool_exists(id_pool=id_pool):
            raise PoolException('Pool with id = {} does not exist'.format(id_pool),
                                code=cs.StatusCodes.not_found_code)
        servers = self._get_pool_servers(id_pool=id_pool)
        for id_server in servers:
            sessions = ServerControl()._get_server_sessions(id_server=id_server)
            SessionControl()._close_list_of_sessions(sessions)
        connection = DBBroker.get_connection(self.config_path)
        sql_text = cs.SQL.set_pool_connected_2_broker.format('FALSE', id_pool)
        DBBroker.send_sql(sql_text, connection)
        connection.commit()
        connection.close()
        return self._get_pool_data(id_pool)

    def _delete_pool(self, id_pool, *args, **kwargs):
        self._disconnect_pool_from_broker(id_pool)
        servers = self._get_pool_servers(id_pool)
        ServerControl()._disconnect_list_of_servers_from_pool(servers)
        users = self._get_pool_users(id_pool=id_pool)
        for id_vdi_user in users:
            user_pool_roles = UserControl()._get_user_pool_roles(id_vdi_user=id_vdi_user, id_pool=id_pool)
            for id_role in user_pool_roles:
                UserControl()._delete_user_role(id_vdi_user=id_vdi_user, id_role=id_role)
        for id_role in self._get_pool_roles(id_pool=id_pool):
            RoleControl()._delete_role(id_role=id_role)
        connection = DBBroker.get_connection(self.config_path)
        sql_text = cs.SQL.delete_pool.format(id_pool)
        DBBroker.send_sql(sql_text, connection)
        connection.commit()
        connection.close()
        return {}

    def _get_pool_data(self, id_pool, *args, **kwargs):
        if not self._pool_exists(id_pool=id_pool):
            raise PoolException('Pool with id = {} does not exist'.format(id_pool),
                                code=cs.StatusCodes.not_found_code)
        connection = DBBroker.get_connection(self.config_path)
        sql_text = cs.SQL.get_pool_data.format(id_pool)
        result = DBBroker.send_select(sql_text, connection)
        connection.close()
        r = APISchema.validate_list_to_dict(list(result[0]), APISchema.get_pool_data_schema)
        r['load'] = 0
        return r

    def _get_pool_list(self, order_by=cs.DefaultSort.order_by.format('pool'),
                       order_dir=cs.DefaultSort.order_dir,
                       limit=cs.DefaultSort.limit, offset=cs.DefaultSort.offset, *args, **kwargs):

        connection = DBBroker.get_connection(self.config_path)

        access = kwargs['access'] if 'access' in kwargs else 'platform'

        if access == 'platform':
            sql_text = cs.SQL.get_pool_list.format(order_by, order_dir, limit, offset)
        else:
            sql_text = cs.SQL.get_pool_list_pool_only.format(' OR '.join(
                ['pool_data.id_pool = {}'.format(id_pool) for id_pool in access]),
                order_by, order_dir, limit, offset)
        result = DBBroker.send_select(sql_text, connection)
        connection.close()
        res = []
        for r in [APISchema.validate_list_to_dict(list(r), APISchema.get_pool_data_schema) for r in result]:
            r['load'] = 0
            res.append(r)
        return res

    @AuthControl.authorized
    @RoleControl.check_role(cs.ROLE.objects.pool, id_action=cs.ROLE.actions.C)
    def create_pool(self, pool_name, description, domain, *args, **kwargs):
        return self._create_pool(pool_name, description, domain, *args, **kwargs)

    @AuthControl.authorized
    @RoleControl.check_role(cs.ROLE.objects.pool)
    def get_pool_data(self, id_pool, *args, **kwargs):
        return self._get_pool_data(id_pool=id_pool, *args, **kwargs)

    @AuthControl.authorized
    @RoleControl.check_role(cs.ROLE.objects.pool)
    def get_pool_list(self, order_by=cs.DefaultSort.order_by.format('pool'), order_dir=cs.DefaultSort.order_dir,
                      limit=cs.DefaultSort.limit, offset=cs.DefaultSort.offset, *args, **kwargs):
        return self._get_pool_list(order_by=order_by,
                                   order_dir=order_dir,
                                   limit=limit, offset=offset, *args, **kwargs)

    @AuthControl.authorized
    @RoleControl.check_role(cs.ROLE.objects.pool, id_action=cs.ROLE.actions.U)
    def connect_pool_to_broker(self, id_pool, *args, **kwargs):
        return self._connect_pool_to_broker(id_pool=id_pool, *args, **kwargs)

    @AuthControl.authorized
    @RoleControl.check_role(cs.ROLE.objects.pool, id_action=cs.ROLE.actions.U)
    def disconnect_pool_from_broker(self, id_pool, *args, **kwargs):
        return self._disconnect_pool_from_broker(id_pool=id_pool, *args, **kwargs)

    @AuthControl.authorized
    @RoleControl.check_role(cs.ROLE.objects.pool, id_action=cs.ROLE.actions.D)
    def delete_pool(self, id_pool, *args, **kwargs):
        return self._delete_pool(id_pool=id_pool, *args, **kwargs)

    @AuthControl.authorized
    @RoleControl.check_role(cs.ROLE.objects.pool, id_action=cs.ROLE.actions.U)
    def connect_server_to_pool(self, id_server=None, id_pool=None, *args, **kwargs):
        return ServerControl(id_token=self.id_token).connect_server_to_pool(id_server=id_server, id_pool=id_pool)

    def _count(self):
        return len(self._get_pool_list())


class ClientControl(DBControl):
    def _get_client(self, id_client=None, client_ip=None, *args, **kwargs):
        n = 'id_client' if id_client is not None else 'client_ip'
        p = id_client if id_client is not None else '\'{}\''.format(client_ip)
        connection = DBBroker.get_connection(self.config_path)
        sql_text = cs.SQL.get_client.format(n, p)
        result = DBBroker.send_select(sql_text, connection)
        connection.close()
        if len(result) == 0:
            raise ClientException('Client with {} = {} does not exist'.format(n, p), code=cs.StatusCodes.not_found_code)
        if len(result) != 1:
            for r in result[1:]:
                self._delete_client(id_client=r['id_client'])

        return APISchema.validate_list_to_dict(list(result[0]), APISchema.get_client_schema)

    def _client_exists(self, id_client=None, client_ip=None, *args, **kwargs):
        try:
            self._get_client(id_client=id_client, client_ip=client_ip, *args, **kwargs)
            return True
        except ClientException as s:
            if s.code == cs.StatusCodes.not_found_code:
                return False
            raise s

    def _create_client(self, client_ip=None, client_mac=None, client_name=None, client_type=None, *args, **kwargs):
        if self._client_exists(client_ip=client_ip):
            raise ClientException('Client with ip = {} already exists'.format(client_ip),
                                  code=cs.StatusCodes.bad_request_code)

        connection = DBBroker.get_connection(self.config_path)
        sql_text = cs.SQL.create_client.format(client_ip, Utils.check_null(client_mac, do_is=False),
                                               Utils.check_null(client_name, do_is=False),
                                               Utils.check_null(client_type, do_is=False))
        DBBroker.send_sql(sql_text, connection)
        connection.commit()
        connection.close()

        return self._get_client(client_ip=client_ip)

    def _delete_client(self, id_client, *args, **kwargs):
        connection = DBBroker.get_connection(self.config_path)
        sql_text = cs.SQL.delete_client.format(id_client)
        DBBroker.send_sql(sql_text, connection)
        connection.commit()
        connection.close()
        return {}

if __name__ == '__main__':
    user_name = 'admin'
    password = '1=qpALzm'
    name = 'Administrator'
    last_name = 'Administartorov'
    email = 'AAAdministratorov@sbcloud.com'

    if not UserControl()._user_exists(user_name=user_name):
        my_user = UserControl()._create_user(user_name=user_name, hashed_password=Utils.hash(password), name=name,
                                             last_name=last_name, email=email)
        print(my_user)
    else:
        my_user = UserControl()._get_user(user_name=user_name)

    if not UserControl()._user_has_role(id_vdi_user=my_user['id_vdi_user'], id_role=1):
        UserControl()._add_user_role(id_vdi_user=my_user['id_vdi_user'], id_role=1)
    if not ServerControl()._server_exists(ip='127.0.0.1'):
        print(
            ServerControl()._create_server(server_name='localhost', ip='127.0.0.1', login='admin', password='1=qpALzm'))

    if not PoolControl()._pool_exists(id_pool=1):
        print(PoolControl()._create_pool(pool_name='test_pool', description='test pool', domain='testdomain.com'))

    id_session = 'admin-{}_{}'.format('-'.join(re.split('[ :.]', str(datetime(2020, 7, 31, 17, 56, 37, 509540)))),
                                      Utils.hash('abc'))
    if not SessionControl()._session_exists(id_session=id_session):
        SessionControl()._create_session(id_session, my_user['id_vdi_user'], 1, 1)

    token = UserControl().sign_in(user_name=user_name,
                                  hashed_pass=Utils.hash(password))[0]['auth_token']
    print(token)
    print(RoleControl(id_token=token).get_role_data(id_role=2))
    print(RoleControl(id_token=token).get_role_list())
    print(UserControl(id_token=token).get_user_data(id_vdi_user=my_user['id_vdi_user']))
    print(UserControl(id_token=token).get_user_list())
    print(ServerControl(id_token=token).get_server_data(id_server=1))
    print(ServerControl(id_token=token).get_server_list())
    print(ServerControl(id_token=token)._get_server_sessions(id_server=1))
    print(PoolControl(id_token=token).get_pool_list())
    print(PoolControl(id_token=token).get_session_list())
    print(UserControl(id_token=token).sign_out())
