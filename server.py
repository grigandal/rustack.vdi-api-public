import os

from flask import Flask, request, json, send_from_directory, send_file
from flask_swagger_ui import get_swaggerui_blueprint

from API import API
from Utils import Response, DBBroker
import Constants as cs

SWAGGER_URL = '/swagger'
API_URL = 'http://185.17.141.230:5001/swagger.json'

app = Flask(__name__)

swaggerui_blueprint = get_swaggerui_blueprint(
    SWAGGER_URL,  # Swagger UI static files will be mapped to '{SWAGGER_URL}/dist/'
    API_URL,
    config={  # Swagger UI config overrides
        'app_name': "Rustack.VDI swagger"
    },
    # oauth_config={  # OAuth config. See https://github.com/swagger-api/swagger-ui#oauth2-configuration .
    #    'clientId': "your-client-id_vdi_user",
    #    'clientSecret': "your-client-secret-if-required",
    #    'realm': "your-realms",
    #    'appName': "your-app-name",
    #    'scopeSeparator': " ",
    #    'additionalQueryStringParams': {'test': "hello"}
    # }
)

app.register_blueprint(swaggerui_blueprint, url_prefix=SWAGGER_URL)


@app.route('/swagger.json', methods=['GET'])
def get_swagger():
    return open('swagger.json').read()


@app.route('/shutdown', methods=['GET'])
def open_shutdown():
    return open('main.html').read()


def shutdown_server():
    func = request.environ.get('werkzeug.server.shutdown')
    if func is None:
        raise RuntimeError('Not running with the Werkzeug Server')
    func()


@app.route('/', methods=['GET'])
def main_page():
    return send_from_directory(app.static_folder, 'index.html')


@app.after_request
def after_request(response):
    return API.after_request(response)


@app.route('/api/shutdown', methods=['POST'])
def shutdown():
    data = json.loads(request.data)
    if data['password'] == '1=qpALzm':
        shutdown_server()
        return Response.success(html='Server shutted down')
    else:
        return Response.error(401, 'Wrong password')


@app.route('/api/', methods=['POST'])
def my_data():
    return API(request.remote_addr, data=request.data, headers=dict(request.headers)).my_data()


@app.route('/api/sign_in', methods=['GET'])
def sign_in():
    return API(request.remote_addr, data=request.data, headers=dict(request.headers)).sign_in()


@app.route('/api/sign_out', methods=['GET'])
def sign_out():
    return API(request.remote_addr, data=request.data, headers=dict(request.headers)).sign_out()


@app.route('/api/users', methods=['GET'])
def get_user_list():
    return API(request.remote_addr, data=request.data, headers=dict(request.headers)).get_user_list()


@app.route('/api/users', methods=['POST'])
def create_user():
    return API(request.remote_addr, data=request.data, headers=dict(request.headers)).create_user()


@app.route('/api/users/<id_vdi_user>', methods=['GET'])
def get_user_data(id_vdi_user):
    return API(request.remote_addr, data=request.data, headers=dict(request.headers)).get_user_data(id_vdi_user)


@app.route('/api/users/<id_vdi_user>', methods=['POST'])
def edit_user(id_vdi_user):
    return API(request.remote_addr, data=request.data, headers=dict(request.headers)).edit_user(id_vdi_user)


@app.route('/api/users/<id_vdi_user>', methods=['DELETE'])
def delete_user(id_vdi_user):
    return API(request.remote_addr, data=request.data, headers=dict(request.headers)).delete_user(id_vdi_user)


@app.route('/api/servers', methods=['GET'])
def servers():
    return API(request.remote_addr, data=request.data, headers=dict(request.headers)).get_server_list()


@app.route('/api/servers/<id_server>', methods=['GET'])
def get_server(id_server):
    return API(request.remote_addr, data=request.data, headers=dict(request.headers)).get_server_data(id_server)


@app.route('/api/servers/<id_server>', methods=['POST'])
def post_server(id_server):
    return API(request.remote_addr, data=request.data, headers=dict(request.headers)).post_server(id_server)


@app.route('/api/servers/<id_server>', methods=['DELETE'])
def delete_server(id_server):
    return API(request.remote_addr, data=request.data, headers=dict(request.headers)).delete_server(id_server)


@app.route('/api/servers', methods=['POST'])
def create_server():
    return API(request.remote_addr, data=request.data, headers=dict(request.headers)).create_server()


@app.route('/api/sessions', methods=['GET'])
def get_all_sessions():
    return API(request.remote_addr, data=request.data, headers=dict(request.headers)).get_sessions_list()


@app.route('/api/sessions/<id_session>', methods=['GET'])
def current_session(id_session):
    return API(request.remote_addr, data=request.data, headers=dict(request.headers)).get_session_data(id_session)


@app.route('/api/sessions/<id_session>', methods=['PUT'])
def block_session(id_session):
    return API(request.remote_addr, data=request.data, headers=dict(request.headers)).block_session(id_session)


@app.route('/api/sessions/<id_session>', methods=['PATCH'])
def unblock_session(id_session):
    return API(request.remote_addr, data=request.data, headers=dict(request.headers)).unblock_session(id_session)


@app.route('/api/sessions/<id_session>', methods=['DELETE'])
def close_session(id_session):
    return API(request.remote_addr, data=request.data, headers=dict(request.headers)).close_session(id_session)


@app.route('/api/pools', methods=['GET'])
def all_pools():
    return API(request.remote_addr, data=request.data, headers=dict(request.headers)).get_pool_list()


@app.route('/api/pools/<id_pool>', methods=['GET'])
def current_pool(id_pool):
    return API(request.remote_addr, data=request.data, headers=dict(request.headers)).get_pool_data(id_pool)


@app.route('/api/pools', methods=['POST'])
def create_pool():
    return API(request.remote_addr, data=request.data, headers=dict(request.headers)).create_pool()


@app.route('/api/pools/<id_pool>', methods=['PUT'])
def put_pool(id_pool):
    return API(request.remote_addr, data=request.data, headers=dict(request.headers)).connect_pool_to_broker(id_pool)


@app.route('/api/pools/<id_pool>', methods=['PATCH'])
def patch_pool(id_pool):
    return API(request.remote_addr, data=request.data, headers=dict(request.headers)).disconnect_pool_from_broker(id_pool)


@app.route('/api/pools/<id_pool>', methods=['DELETE'])
def remove_pool(id_pool):
    return API(request.remote_addr, data=request.data, headers=dict(request.headers)).delete_pool(id_pool)


@app.route('/api/roles', methods=['GET'])
def all_roles():
    return API(request.remote_addr, data=request.data, headers=dict(request.headers)).get_role_list()


@app.route('/api/roles/<id_role>', methods=['GET'])
def current_role(id_role):
    return API(request.remote_addr, data=request.data, headers=dict(request.headers)).get_role_data(id_role=id_role)


@app.route('/api/roles', methods=['POST'])
def create_role():
    return API(request.remote_addr, data=request.data, headers=dict(request.headers)).create_role()


@app.route('/api/roles/<id_role>', methods=['PATCH'])
def edit_role(id_role):
    return API(request.remote_addr, data=request.data, headers=dict(request.headers)).edit_role(id_role=id_role)


@app.route('/api/roles/<id_role>', methods=['DELETE'])
def delete_role(id_role):
    return API(request.remote_addr, data=request.data, headers=dict(request.headers)).delete_role(id_role=id_role)


@app.route('/api/permissions', methods=['GET'])
def all_permissions():
    return API(request.remote_addr, data=request.data, headers=dict(request.headers)).get_permission_list()


@app.route('/api/permissions', methods=['POST'])
def create_permission():
    return API(request.remote_addr, data=request.data, headers=dict(request.headers)).create_permission()


@app.route('/api/permissions/<id_permission>', methods=['GET'])
def current_permission(id_permission):
    return API(request.remote_addr, data=request.data, headers=dict(request.headers)).get_permission_data(id_permission=id_permission)


@app.route('/api/permissions/<id_permission>', methods=['PATCH'])
def edit_permission(id_permission):
    return API(request.remote_addr, data=request.data, headers=dict(request.headers)).edit_permission(id_permission=id_permission)


@app.route('/api/permissions/<id_permission>', methods=['DELETE'])
def delete_permission(id_permission):
    return API(request.remote_addr, data=request.data, headers=dict(request.headers)).delete_permission(id_permission=id_permission)


@app.route('/api/journal', methods=['GET'])
def get_journal_records():
    return API(request.remote_addr, data=request.data, headers=dict(request.headers)).get_journal_records()

if __name__ == "__main__":
    config = DBBroker.read_config(cs.Config.host_config)
    app.run(config['host'], config['port'])
    # app.run(debug=True)
