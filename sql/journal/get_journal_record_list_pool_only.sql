SELECT id_journal_record,
       vdi_user.id_vdi_user,
       vdi_user.user_name,
       journal_record.id_client,
       client.client_ip,
       client.client_name,
       id_object,
       id_action,
       current_object,
       journal_record.created_at,
       success,
       description
FROM   journal_record,
       vdi_user,
       role_assignment,
       role
WHERE  journal_record.id_vdi_user = vdi_user.id_vdi_user
       AND role_assignment.id_vdi_user = vdi_user.id_vdi_user
       AND role_assignment.id_role = role.id_role
       AND ({})
       ORDER BY {} {}
       LIMIT    {}
       OFFSET   {};