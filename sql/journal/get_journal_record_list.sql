SELECT id_journal_record,
       vdi_user.id_vdi_user,
       vdi_user.user_name,
       journal_record.id_client,
       client.client_ip,
       client.client_name,
       id_object,
       id_action,
       current_object,
       journal_record.created_at,
       success,
       description
FROM   journal_record,
       vdi_user,
       client
WHERE  client.id_client = journal_record.id_client
       AND journal_record.id_vdi_user = vdi_user.id_vdi_user
       ORDER BY {} {}
       LIMIT    {}
       OFFSET   {};