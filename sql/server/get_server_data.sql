SELECT DISTINCT server.id_server,
       server.server_name,
       server.ip,
       active.active,
       suspend.suspend,
       blocked.blocked,
       server.id_pool,
       CASE
            WHEN server.id_pool IS NULL THEN FALSE
            ELSE pool.connected_2_broker
       END as connected_2_broker,
       CASE
            WHEN server.id_pool IS NULL THEN NULL
            ELSE pool.pool_name
       END as pool_name
FROM   (server
        LEFT JOIN pool
            ON (server.id_server is NULL or pool.id_pool = server.id_pool)
       ),
       (SELECT DISTINCT server.id_server,
               Count(session.id_session) AS active
        FROM   server
               LEFT JOIN session
                      ON server.id_server = session.id_server
                         AND session.state = 1
        GROUP  BY server.id_server) AS active,
       (SELECT DISTINCT server.id_server,
               Count(session.id_session) AS suspend
        FROM   server
               LEFT JOIN session
                      ON server.id_server = session.id_server
                         AND session.state = 2
        GROUP  BY server.id_server) AS suspend,
       (SELECT DISTINCT server.id_server,
               Count(session.id_session) AS blocked
        FROM   server
               LEFT JOIN session
                      ON server.id_server = session.id_server
                         AND session.state = 3
        GROUP  BY server.id_server) AS blocked
WHERE  active.id_server = server.id_server
       AND suspend.id_server = server.id_server
       AND blocked.id_server = server.id_server
       AND server.id_server = {};