SELECT DISTINCT session.id_session,
                session.id_client,
                session.id_vdi_user,
                session.id_server,
                session.created_at,
                session.updated_at,
                session.state,
                server.server_name,
                vdi_user.user_name,
                client.client_ip,
                client.client_name
FROM            session,
                server,
                vdi_user,
                client
WHERE           session.id_server = server.id_server
                AND session.id_client = client.id_client
                AND session.id_vdi_user = vdi_user.id_vdi_user
ORDER BY {} {}
LIMIT    {}
OFFSET   {};