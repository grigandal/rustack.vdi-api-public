ALTER TABLE IF EXISTS excepted_permission DROP CONSTRAINT IF EXISTS id_permission CASCADE;
ALTER TABLE IF EXISTS excepted_permission DROP CONSTRAINT IF EXISTS id_vdi_user CASCADE;
ALTER TABLE IF EXISTS journal_record DROP CONSTRAINT IF EXISTS fk_journal_record_vdi_user_1 CASCADE;
ALTER TABLE IF EXISTS journal_record DROP CONSTRAINT IF EXISTS fk_journal_record_object_1 CASCADE;
ALTER TABLE IF EXISTS journal_record DROP CONSTRAINT IF EXISTS fk_journal_record_action_1 CASCADE;
ALTER TABLE IF EXISTS journal_record DROP CONSTRAINT IF EXISTS fk_journal_record_client_1 CASCADE;
ALTER TABLE IF EXISTS permission DROP CONSTRAINT IF EXISTS id_action CASCADE;
ALTER TABLE IF EXISTS permission DROP CONSTRAINT IF EXISTS id_object CASCADE;
ALTER TABLE IF EXISTS role DROP CONSTRAINT IF EXISTS id_pool CASCADE;
ALTER TABLE IF EXISTS role DROP CONSTRAINT IF EXISTS id_role_template CASCADE;
ALTER TABLE IF EXISTS role_assignment DROP CONSTRAINT IF EXISTS id_role CASCADE;
ALTER TABLE IF EXISTS role_assignment DROP CONSTRAINT IF EXISTS id_vdi_user CASCADE;
ALTER TABLE IF EXISTS role_permission DROP CONSTRAINT IF EXISTS id_permission CASCADE;
ALTER TABLE IF EXISTS role_permission DROP CONSTRAINT IF EXISTS id_role_template CASCADE;
ALTER TABLE IF EXISTS role_template DROP CONSTRAINT IF EXISTS id_role_type CASCADE;
ALTER TABLE IF EXISTS server DROP CONSTRAINT IF EXISTS id_pool CASCADE;
ALTER TABLE IF EXISTS session DROP CONSTRAINT IF EXISTS id_client CASCADE;
ALTER TABLE IF EXISTS session DROP CONSTRAINT IF EXISTS id_server CASCADE;
ALTER TABLE IF EXISTS session DROP CONSTRAINT IF EXISTS id_vdi_user CASCADE;
ALTER TABLE IF EXISTS session DROP CONSTRAINT IF EXISTS state CASCADE;
ALTER TABLE IF EXISTS token DROP CONSTRAINT IF EXISTS id_vdi_user CASCADE;

ALTER TABLE IF EXISTS action DROP CONSTRAINT IF EXISTS action_pkey CASCADE;
ALTER TABLE IF EXISTS client DROP CONSTRAINT IF EXISTS client_pkey CASCADE;
ALTER TABLE IF EXISTS excepted_permission DROP CONSTRAINT IF EXISTS excepted_permission_pkey CASCADE;
ALTER TABLE IF EXISTS object DROP CONSTRAINT IF EXISTS object_pkey CASCADE;
ALTER TABLE IF EXISTS permission DROP CONSTRAINT IF EXISTS permission_pkey CASCADE;
ALTER TABLE IF EXISTS pool DROP CONSTRAINT IF EXISTS pool_pkey CASCADE;
ALTER TABLE IF EXISTS role DROP CONSTRAINT IF EXISTS role_instance_pkey CASCADE;
ALTER TABLE IF EXISTS role_assignment DROP CONSTRAINT IF EXISTS role_assignment_pkey CASCADE;
ALTER TABLE IF EXISTS role_permission DROP CONSTRAINT IF EXISTS role_permission_pkey CASCADE;
ALTER TABLE IF EXISTS role_template DROP CONSTRAINT IF EXISTS role_pkey CASCADE;
ALTER TABLE IF EXISTS role_type DROP CONSTRAINT IF EXISTS role_type_pkey CASCADE;
ALTER TABLE IF EXISTS server DROP CONSTRAINT IF EXISTS server_pkey CASCADE;
ALTER TABLE IF EXISTS session DROP CONSTRAINT IF EXISTS session_pkey CASCADE;
ALTER TABLE IF EXISTS session_state DROP CONSTRAINT IF EXISTS session_state_pkey CASCADE;
ALTER TABLE IF EXISTS token DROP CONSTRAINT IF EXISTS token_pkey CASCADE;
ALTER TABLE IF EXISTS vdi_user DROP CONSTRAINT IF EXISTS vdi_user_pkey CASCADE;

DROP TABLE IF EXISTS action CASCADE;
DROP TABLE IF EXISTS client CASCADE;
DROP TABLE IF EXISTS excepted_permission CASCADE;
DROP TABLE IF EXISTS journal_record CASCADE;
DROP TABLE IF EXISTS object CASCADE;
DROP TABLE IF EXISTS permission CASCADE;
DROP TABLE IF EXISTS pool CASCADE;
DROP TABLE IF EXISTS role CASCADE;
DROP TABLE IF EXISTS role_assignment CASCADE;
DROP TABLE IF EXISTS role_permission CASCADE;
DROP TABLE IF EXISTS role_template CASCADE;
DROP TABLE IF EXISTS role_type CASCADE;
DROP TABLE IF EXISTS server CASCADE;
DROP TABLE IF EXISTS session CASCADE;
DROP TABLE IF EXISTS session_state CASCADE;
DROP TABLE IF EXISTS token CASCADE;
DROP TABLE IF EXISTS vdi_user CASCADE;

CREATE TABLE action (
  id_action SERIAL NOT NULL,
  action_name varchar(255) NOT NULL,
  CONSTRAINT action_pkey PRIMARY KEY (id_action)
);

CREATE TABLE client (
  id_client SERIAL NOT NULL,
  client_ip varchar(255) NOT NULL,
  client_mac varchar(255),
  client_name varchar(255),
  client_type varchar(255),
  CONSTRAINT client_pkey PRIMARY KEY (id_client)
);

CREATE TABLE excepted_permission (
  id_excepted_permission SERIAL NOT NULL,
  id_permission int NOT NULL,
  id_vdi_user int NOT NULL,
  CONSTRAINT excepted_permission_pkey PRIMARY KEY (id_excepted_permission)
);

CREATE TABLE journal_record (
  id_journal_record SERIAL NOT NULL,
  id_vdi_user int,
  id_client int NOT NULL,
  id_object int,
  id_action int,
  current_object text,
  created_at timestamp NOT NULL,
  success bool NOT NULL,
  description text NOT NULL,
  PRIMARY KEY (id_journal_record)
);

CREATE TABLE object (
  id_object SERIAL NOT NULL,
  object_name varchar(255) NOT NULL,
  CONSTRAINT object_pkey PRIMARY KEY (id_object)
);

CREATE TABLE permission (
  id_permission SERIAL NOT NULL,
  id_object int NOT NULL,
  id_action int NOT NULL,
  permission_name VARCHAR(255),
  CONSTRAINT permission_pkey PRIMARY KEY (id_permission)
);

CREATE TABLE pool (
  id_pool SERIAL NOT NULL,
  pool_name varchar(255),
  connected_2_broker bool NOT NULL,
  description text NOT NULL,
  domain varchar(255) NOT NULL,
  CONSTRAINT pool_pkey PRIMARY KEY (id_pool)
);

CREATE TABLE role (
  id_role SERIAL NOT NULL,
  id_pool int,
  id_role_template int NOT NULL,
  role_name varchar(255) NOT NULL,
  CONSTRAINT role_instance_pkey PRIMARY KEY (id_role)
);

CREATE TABLE role_assignment (
  id_role_assignment SERIAL NOT NULL,
  id_role int NOT NULL,
  id_vdi_user int NOT NULL,
  CONSTRAINT role_assignment_pkey PRIMARY KEY (id_role_assignment)
);

CREATE TABLE role_permission (
  id_role_permission SERIAL NOT NULL,
  id_permission int NOT NULL,
  id_role_template int NOT NULL,
  CONSTRAINT role_permission_pkey PRIMARY KEY (id_role_permission)
);

CREATE TABLE role_template (
  id_role_template SERIAL NOT NULL,
  id_role_type int NOT NULL,
  CONSTRAINT role_pkey PRIMARY KEY (id_role_template)
);

CREATE TABLE role_type (
  id_role_type SERIAL NOT NULL,
  role_type_name varchar(255) NOT NULL,
  CONSTRAINT role_type_pkey PRIMARY KEY (id_role_type)
);

CREATE TABLE server (
  id_server SERIAL NOT NULL,
  server_name varchar(255) NOT NULL,
  ip varchar(255) NOT NULL,
  login varchar(255) NOT NULL,
  password varchar(255) NOT NULL,
  ad_login varchar(255),
  ad_password varchar(255),
  id_pool int,
  CONSTRAINT server_pkey PRIMARY KEY (id_server)
);

CREATE TABLE session (
  id_session varchar(255) NOT NULL,
  id_vdi_user int NOT NULL,
  id_client int NOT NULL,
  id_server int NOT NULL,
  created_at timestamp(6) NOT NULL,
  updated_at timestamp(6),
  state int NOT NULL,
  CONSTRAINT session_pkey PRIMARY KEY (id_session)
);

CREATE TABLE session_state (
  id_state SERIAL NOT NULL,
  name varchar(255),
  CONSTRAINT session_state_pkey PRIMARY KEY (id_state)
);

CREATE TABLE token (
  id_token varchar(255) NOT NULL,
  id_vdi_user int NOT NULL,
  created_at timestamp(6) NOT NULL,
  expired_at timestamp(6),
  CONSTRAINT token_pkey PRIMARY KEY (id_token)
);

CREATE TABLE vdi_user (
  id_vdi_user SERIAL NOT NULL,
  name varchar(255),
  last_name varchar(255),
  email varchar(255),
  user_name varchar(255) NOT NULL,
  password text NOT NULL,
  state varchar(255),
  created_at timestamp(6),
  CONSTRAINT vdi_user_pkey PRIMARY KEY (id_vdi_user)
);

ALTER TABLE excepted_permission ADD CONSTRAINT id_permission FOREIGN KEY (id_permission) REFERENCES permission (id_permission) ON DELETE RESTRICT ON UPDATE RESTRICT;
ALTER TABLE excepted_permission ADD CONSTRAINT id_vdi_user FOREIGN KEY (id_vdi_user) REFERENCES vdi_user (id_vdi_user) ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE journal_record ADD CONSTRAINT fk_journal_record_vdi_user_1 FOREIGN KEY (id_vdi_user) REFERENCES vdi_user (id_vdi_user);
ALTER TABLE journal_record ADD CONSTRAINT fk_journal_record_object_1 FOREIGN KEY (id_object) REFERENCES object (id_object);
ALTER TABLE journal_record ADD CONSTRAINT fk_journal_record_action_1 FOREIGN KEY (id_action) REFERENCES action (id_action);
ALTER TABLE journal_record ADD CONSTRAINT fk_journal_record_client_1 FOREIGN KEY (id_client) REFERENCES client (id_client);
ALTER TABLE permission ADD CONSTRAINT id_action FOREIGN KEY (id_action) REFERENCES action (id_action) ON DELETE RESTRICT ON UPDATE RESTRICT;
ALTER TABLE permission ADD CONSTRAINT id_object FOREIGN KEY (id_object) REFERENCES object (id_object) ON DELETE RESTRICT ON UPDATE RESTRICT;
ALTER TABLE role ADD CONSTRAINT id_role_template FOREIGN KEY (id_role_template) REFERENCES role_template (id_role_template) ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE role ADD CONSTRAINT id_pool FOREIGN KEY (id_pool) REFERENCES pool (id_pool) ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE role_assignment ADD CONSTRAINT id_role FOREIGN KEY (id_role) REFERENCES role (id_role) ON DELETE RESTRICT ON UPDATE RESTRICT;
ALTER TABLE role_assignment ADD CONSTRAINT id_vdi_user FOREIGN KEY (id_vdi_user) REFERENCES vdi_user (id_vdi_user) ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE role_permission ADD CONSTRAINT id_role_template FOREIGN KEY (id_role_template) REFERENCES role_template (id_role_template) ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE role_permission ADD CONSTRAINT id_permission FOREIGN KEY (id_permission) REFERENCES permission (id_permission) ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE role_template ADD CONSTRAINT id_role_type FOREIGN KEY (id_role_type) REFERENCES role_type (id_role_type) ON DELETE RESTRICT ON UPDATE RESTRICT;
ALTER TABLE server ADD CONSTRAINT id_pool FOREIGN KEY (id_pool) REFERENCES pool (id_pool) ON DELETE SET NULL ON UPDATE CASCADE;
ALTER TABLE session ADD CONSTRAINT id_client FOREIGN KEY (id_client) REFERENCES client (id_client) ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE session ADD CONSTRAINT id_server FOREIGN KEY (id_server) REFERENCES server (id_server) ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE session ADD CONSTRAINT id_vdi_user FOREIGN KEY (id_vdi_user) REFERENCES vdi_user (id_vdi_user) ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE session ADD CONSTRAINT state FOREIGN KEY (state) REFERENCES session_state (id_state) ON DELETE RESTRICT ON UPDATE RESTRICT;
ALTER TABLE token ADD CONSTRAINT id_vdi_user FOREIGN KEY (id_vdi_user) REFERENCES vdi_user (id_vdi_user) ON DELETE CASCADE ON UPDATE CASCADE;

INSERT INTO object(object_name)
VALUES ('pool'),
       ('server'),
       ('user'),
       ('session');

INSERT INTO action(action_name)
VALUES ('C'),
       ('R'),
       ('U'),
       ('D'),
       ('A'),
       ('DA');

INSERT INTO role_type(role_type_name)
VALUES ('platform'),
       ('pool');

INSERT INTO role_template(id_role_type)
VALUES (1),
       (1),
       (2),
       (2);

INSERT INTO permission(id_object, id_action, permission_name)
VALUES (1, 1, 'Создание пула'),
       (1, 2, 'Чтение пула (списка пулов)'),
       (1, 3, 'Изменение пула'),
       (1, 4, 'Удаление пула'),
       (2, 1, 'Создание сервера'),
       (2, 2, 'Чтение сервера (списка серверов)'),
       (2, 3, 'Изменение сервера'),
       (2, 4, 'Удаление сервера'),
       (3, 1, 'Создание пользователя'),
       (3, 2, 'Чтение пользователя (списка пользователей)'),
       (3, 3, 'Изменение пользователя'),
       (3, 4, 'Удаление пользователя'),
       (4, 1, 'Создание сессии'),
       (4, 2, 'Чтение сессий (списка сессий)'),
       (4, 3, 'Изменение сессии'),
       (4, 4, 'Удаление сессии');

INSERT INTO role_permission(id_permission, id_role_template)
VALUES (1, 1),
       (2, 1),
       (3, 1),
       (4, 1),
       (5, 1),
       (6, 1),
       (7, 1),
       (8, 1),
       (9, 1),
       (10, 1),
       (11, 1),
       (12, 1),
       (14, 1),
       (15, 1),
       (16, 1),

       (1, 2),
       (2, 2),
       (3, 2),
       (4, 2),
       (5, 2),
       (6, 2),
       (7, 2),
       (8, 2),

       (2, 3),
       (5, 3),
       (6, 3),
       (7, 3),
       (8, 3),
       (10, 3),
       (11, 3),
       (12, 3),
       (15, 3),
       (16, 3),

       (2, 4),
       (5, 4),
       (6, 4),
       (7, 4),
       (8, 4);

INSERT INTO session_state (name)
VALUES ('active'),
       ('suspended'),
       ('blocked');

INSERT INTO client(client_ip, client_mac, client_name, client_type)
VALUES ('1.1.1.1', '11-AA-11-AA-AA-11', 'HP-ProBook', 'PC');

INSERT INTO pool(pool_name, connected_2_broker, description, domain)
VALUES ('test_pool', FALSE, 'Testing', 'test.com');

INSERT INTO role (id_pool, id_role_template, role_name)
VALUES (NULL, 1, 'Администратор платформы'),
       (NULL, 2, 'Инженер платформы'),
       (1, 3, 'Администратор пула "test_pool"'),
       (1, 3, 'Инженер пула "test_pool"');

