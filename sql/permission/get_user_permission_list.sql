SELECT role.id_pool,
       permission.id_permission,
       permission.id_object,
       permission.id_action
FROM   role_assignment,
       role,
       role_template,
       role_permission,
       permission
WHERE  role_assignment.id_role = role.id_role
       AND role.id_role_template = role_template.id_role_template
       AND role_permission.id_role_template = role_template.id_role_template
       AND role_permission.id_permission = permission.id_permission
       AND role_assignment.id_vdi_user = {}