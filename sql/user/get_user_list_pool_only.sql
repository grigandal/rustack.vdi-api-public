SELECT DISTINCT vdi_user.id_vdi_user,
       vdi_user.name,
       last_name,
       email,
       user_name,
       created_at
FROM   vdi_user, role_assignment, role_instance
WHERE  role_assignment.id_vdi_user = vdi_user.id_vdi_user
       AND role_assignment.id_role = role_instance.id_role
       AND {}
ORDER BY {} {}
LIMIT    {}
OFFSET   {};