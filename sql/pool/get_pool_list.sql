SELECT pool.id_pool,
       pool_name,
       connected_2_broker,
       description,
       domain,
       Count(server.id_server) as servers
FROM   (pool
        LEFT JOIN server
            ON server.id_pool = pool.id_pool)
GROUP BY pool.id_pool
ORDER BY {} {}
LIMIT    {}
OFFSET   {};