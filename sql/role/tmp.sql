SELECT id_vdi_user,
       id_pool,
	   role_instance.id_role,
	   id_role_type,
	   object.id_object,
	   action.id_action
FROM   role_assignment,
       role_instance,
	   role,
	   role_permission,
	   permission,
	   object,
	   action
WHERE  role_assignment.id_role = role_instance.id_role
       AND role_instance.id_role = role.id_role
       AND role.id_role = role_permission.id_role
       AND role_permission.id_permission = permission.id_permission
	   AND permission.id_object = object.id_object
	   AND permission.id_action = action.id_action;
