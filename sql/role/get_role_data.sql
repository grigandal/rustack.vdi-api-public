SELECT DISTINCT pool_role.id_role,
                pool_role.role_name,
                pool_role.id_pool,
                pool_role.pool_name,
                role_template.id_role_type,
                permission.id_permission,
                permission.permission_name,
	            object.id_object,
	            action.id_action
FROM            (SELECT role.id_role,
                        role.role_name,
                        pool.id_pool,
                        role.id_role_template,
                        CASE
                            WHEN role.id_pool IS NULL THEN NULL
                            ELSE pool.pool_name
                        END as pool_name
                 FROM   role
                        LEFT JOIN pool
                             ON role.id_pool = pool.id_pool
                 GROUP BY pool.id_pool, role.id_role, role.role_name, role.id_role_template, role.id_pool) AS pool_role,
                role_template,
	            role_permission,
	            permission,
	            object,
	            action
WHERE           pool_role.id_role_template = role_template.id_role_template
                AND role_template.id_role_template = role_permission.id_role_template
                AND role_permission.id_permission = permission.id_permission
	            AND permission.id_object = object.id_object
	            AND permission.id_action = action.id_action
                AND pool_role.id_role = {};