SELECT DISTINCT role.id_role,
                role.role_name,
				role.id_pool,
				CASE
                    WHEN role.id_pool is NULL THEN NULL
                    ELSE role.pool_name
                END as pool_name
FROM            role_assignment,
                (SELECT role.id_role, role.role_name, role.id_pool, pool.pool_name FROM role LEFT JOIN pool
                     ON role.id_pool = pool.id_pool
                    GROUP BY role.id_role, role.role_name, role.id_pool, pool.pool_name) as role
WHERE           role_assignment.id_role = role.id_role
				AND role_assignment.id_vdi_user={};