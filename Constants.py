import glob
import os


class FieldedObject:
    def __init__(self, **kwargs):
        for p in kwargs:
            self.__setattr__(p, kwargs[p])


class StatusCodes:
    continue_code = 100
    switching_protocol_code = 101
    processing_code = 102
    early_hints_code = 103
    ok_code = 200
    created_code = 201
    accepted_code = 202
    non_authoritative_information_code = 203
    no_content_code = 204
    reset_content_code = 205
    partial_content_code = 206
    multiple_choice_code = 300
    moved_permanently_code = 301
    found_code = 302
    see_other_code = 303
    not_modified_code = 304
    use_proxy_code = 305
    switch_proxy_code = 306
    temporary_redirect_code = 307
    permanent_redirect_code = 308
    bad_request_code = 400
    unauthorized_code = 401
    payment_required_code = 402
    forbidden_code = 403
    not_found_code = 404
    method_not_allowed_code = 405
    not_acceptable_code = 406
    proxy_authentication_required_code = 407
    request_timeout_code = 408
    conflict_code = 409
    gone_code = 410
    length_required_code = 411
    precondition_failed_code = 412
    request_entity_too_large_code = 413
    request_uri_too_long_code = 414
    unsupported_media_type_code = 415
    requested_range_not_satisfiable_code = 416
    expectation_failed_code = 417
    internal_server_error_code = 500
    not_implemented_code = 501
    bad_gateway_code = 502
    service_unavailable_code = 503
    gateway_timeout_code = 504
    http_version_not_supported_code = 505


class Config:
    base_config = 'psgconfig'
    su_config = 'su-psgconfig'
    data_config = 'data-psgconfig'
    host_config = 'hostconfig'


class SQLPaths:
    def __getattr__(self, name):
        all_sql = [{
            'path': e.replace('\\', '/'),
            'name': os.path.split(e)[-1]
        } for e in glob.glob('sql/*.sql') + glob.glob('sql/*/*.sql') if os.path.split(e)[-1][:-4] == name]
        if len(all_sql) > 1:
            from Exceptions import FindSQLError
            raise FindSQLError('Found more than one SQL file with name \'{}\':\n{}'.format(name, '\n'.join(
                [e['path'] for e in all_sql])))
        return open(all_sql[0]['path']).read() if len(all_sql) == 1 else None


SQL = SQLPaths()


class DefaultSort:
    order_by = 'id_{}'
    order_dir = 'ASC'
    limit = 'ALL'
    offset = 0
    page = 0


class SState:
    active = 1
    suspend = 2
    blocked = 3


class ROLE:
    objects = FieldedObject(pool=1, server=2, user=3, session=4, object_name=['pool', 'server', 'vdi_user', 'session'])
    actions = FieldedObject(C=1, R=2, U=3, D=4, auth=5, deauth=6, action_name=['create', 'read', 'update', 'delete',
                                                                               'authorization', 'deauthorization'])
    type = FieldedObject(platform=1, pool=2)


class Properties:
    create_server_props = [
        {
            'name': 'server',
            'desc': 'DNS name'
        },
        {
            'name': 'ip',
            'desc': 'IP address'
        },
        {
            'name': 'login',
            'desc': 'VM administrator login'
        },
        {
            'name': 'password',
            'desc': 'VM administrator password'
        },
    ]

    post_server_props = [
        {'name': 'method'}
    ]

    post_server_methods = {
        'setup': 'customize_server',
        'put': 'put_server',
        'pop': 'pop_server'
    }
